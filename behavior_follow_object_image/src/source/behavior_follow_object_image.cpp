/*!*******************************************************************************************
 *  \file       behavior_follow_object_image.cpp
 *  \brief      Behavior Follow Object Image implementation file.
 *  \details    This file implements the behaviorFollowObjectImage class.
 *  \authors    Rafael Arti�ano Mu�oz
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/behavior_follow_object_image.h"
behaviorFollowObjectImage::behaviorFollowObjectImage():BehaviorProcess()
{

}

behaviorFollowObjectImage::~behaviorFollowObjectImage()
{

}

void behaviorFollowObjectImage::ownSetUp()
{
  std::cout << "ownSetup" << std::endl;

  node_handle.param<std::string>("drone_id", drone_id, "1");
  node_handle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  node_handle.param<std::string>("my_stack_directory", my_stack_directory,
                  "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");

  node_handle.param<std::string>("estimated_pose_topic", estimated_pose_str, "estimated_pose");
  node_handle.param<std::string>("controllers_topic", controllers_str, "command/high_level");
  node_handle.param<std::string>("rotation_angles_topic", rotation_angles_str, "rotation_angles");
  node_handle.param<std::string>("estimated_speed_topic",estimated_speed_str,"estimated_speed");
  node_handle.param<std::string>("yaw_controller_str",yaw_controller_str , "droneControllerYawRefCommand");
  node_handle.param<std::string>("service_topic_str",service_topic_str , "droneTrajectoryController/setControlMode");
  node_handle.param<std::string>("drone_position_str",drone_position_str , "dronePositionRefs");
  node_handle.param<std::string>("mission_point",mission_point, "droneMissionPoint");
  node_handle.param<std::string>("object_on_frame",object_on_frame_str, "is_object_on_frame");
  node_handle.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
  node_handle.param<std::string>("opentld_bb",tld_bb_srv,"tld_gui_bb");
}

void behaviorFollowObjectImage::ownStart()
{
  std::cout << "ownStart" << std::endl;
  is_finished = false;

  /*Initialize topics*/
    estimated_pose_sub = node_handle.subscribe(estimated_pose_str, 1000, &behaviorFollowObjectImage::estimatedPoseCallBack, this);
    rotation_angles_sub = node_handle.subscribe(rotation_angles_str, 1000, &behaviorFollowObjectImage::rotationAnglesCallback, this);
    estimated_speed_sub = node_handle.subscribe(estimated_speed_str, 1000, &behaviorFollowObjectImage::estimatedSpeedCallback, this);
    controllers_pub = node_handle.advertise<droneMsgsROS::droneCommand>(controllers_str, 1, true);
    yaw_controller_pub=node_handle.advertise<droneMsgsROS::droneYawRefCommand>(yaw_controller_str,1000);
    mode_service=node_handle.serviceClient<droneMsgsROS::setControlMode>(service_topic_str);
    drone_position_pub=node_handle.advertise< droneMsgsROS::dronePositionRefCommandStamped>(drone_position_str,1000);
    pose_topic_pub=node_handle.advertise<droneMsgsROS::dronePositionRefCommand>(mission_point,1000);
    object_on_frame=node_handle.subscribe(object_on_frame_str,1000,&behaviorFollowObjectImage::isObjectOnFrameCallback,this);
    query_client = node_handle.serviceClient <droneMsgsROS::ConsultBelief> (execute_query_srv);
    tld_service_pub=node_handle.advertise<tld_msgs::Target>(tld_bb_srv,1000);
    std::string arguments = getArguments();
    YAML::Node config_file = YAML::Load(arguments);
    std::string name;
    if(config_file["name"].IsDefined()){
      name=config_file["name"].as<std::string>();

      }
      else {
        setStarted(false);
        return;
      }
    /*estimated_pose_msg = *ros::topic::waitForMessage<droneMsgsROS::dronePose>(estimated_pose_str, node_handle, ros::Duration(2));

  behavior implementation

    droneMsgsROS::dronePositionRefCommandStamped command;
    command.position_command.x=estimated_pose_msg.x;
    command.position_command.y=estimated_pose_msg.y;
    command.position_command.z=estimated_pose_msg.z;
    drone_position_pub.publish(command);
    droneMsgsROS::droneYawRefCommand new_yaw;
    new_yaw.yaw=estimated_pose_msg.yaw;
    yaw_controller_pub.publish(new_yaw);
    droneMsgsROS::setControlMode  mode;
    mode.request.controlMode.command=mode.request.controlMode.PBVS_TRACKER_IS_REFERENCE;
    mode_service.call(mode);*/

   rosbag::Bag bag;
   std::string f="/configs/general/"+name+".bag";
   std::string file=getenv("AEROSTACK_STACK")+f ;
   bag.open(file, rosbag::bagmode::Read);
   std::vector<std::string> topics;
   topics.push_back(std::string("tld_gui_bb"));
   rosbag::View view(bag, rosbag::TopicQuery(topics));
   tld_msgs::Target::ConstPtr s;
   foreach(rosbag::MessageInstance const m, view)
   {
     s = m.instantiate<tld_msgs::Target>();

  }

    bag.close();
    tld_service_pub.publish(*s);
    drone_mode.command = droneMsgsROS::droneCommand::MOVE;
    controllers_pub.publish(drone_mode);
    //staticity_timer.time(5);
    static_pose.x=estimated_pose_msg.x;
    static_pose.y=estimated_pose_msg.y;
    static_pose.z=estimated_pose_msg.z;
    std::cout<< "el tiempo "<< getTimeout() << std::endl;
}

void behaviorFollowObjectImage::ownRun()
{
  if(!seeing_object && drone_mode.command != droneMsgsROS::droneCommand::HOVER )
  {
    drone_mode.command = droneMsgsROS::droneCommand::HOVER;
    controllers_pub.publish(drone_mode);
  }
  else if(drone_mode.command != droneMsgsROS::droneCommand::MOVE)
  {

    drone_mode.command = droneMsgsROS::droneCommand::MOVE;
    controllers_pub.publish(drone_mode);
  }

  if(is_finished==false)
  {
   if(timerIsFinished())
   {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
   }
   /*if(staticity_timer.isFinished() && std::abs(static_pose.z-estimated_pose_msg.z)<pose_variation_maximum && std::abs(static_pose.x-estimated_pose_msg.x)<pose_variation_maximum && std::abs(static_pose.y- estimated_pose_msg.y)<pose_variation_maximum)
   {
      BehaviorProcess::setFinishEvent(droneMsgsROS::behaviorEvent::BLOCKED);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
   }
   if(std::abs(target_position.z-estimated_pose_msg.z)<pose_variation_maximum && std::abs(target_position.x-estimated_pose_msg.x)<pose_variation_maximum && std::abs(target_position.y- estimated_pose_msg.y)<pose_variation_maximum && estimated_speed_msg.dx>speed_maximum_at_end && estimated_speed_msg.dz>speed_maximum_at_end && estimated_speed_msg.dy>speed_maximum_at_end)
   {
      BehaviorProcess::setFinishEvent(droneMsgsROS::behaviorEvent::WRONG_PROGRESS);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
   }*/
  }


}
std::tuple<bool,std::string> behaviorFollowObjectImage::ownCheckActivationConditions()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador << "battery_level(self,LOW)";
  std::string query(capturador.str());
  query_service.request.query = query;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Battery low, unable to perform action");
    //return false;
  }
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,LANDED)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Drone landed");
    //return false;
  }

  return std::make_tuple(true,"");
  //return true;
}
void behaviorFollowObjectImage::ownStop()
{
  droneMsgsROS::droneCommand msg;
  msg.command = droneMsgsROS::droneCommand::HOVER;
  controllers_pub.publish(msg);
  estimated_pose_sub.shutdown();
  rotation_angles_sub.shutdown();
  estimated_speed_sub.shutdown();
  //staticity_timer.stop();
}
void behaviorFollowObjectImage::estimatedSpeedCallback(const droneMsgsROS::droneSpeeds& msg)
{
estimated_speed_msg=msg;
}
void behaviorFollowObjectImage::estimatedPoseCallBack(const droneMsgsROS::dronePose& msg)
{
estimated_pose_msg=msg;
}
void behaviorFollowObjectImage::rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg)
{
rotation_angles_msg=msg;
}
void behaviorFollowObjectImage::isObjectOnFrameCallback(const std_msgs::Bool &msg)
{
    seeing_object = msg.data;
}
