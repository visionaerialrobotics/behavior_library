#include "../include/behavior_follow_path.h"
BehaviorFollowPath::BehaviorFollowPath():BehaviorProcess()
{

}

BehaviorFollowPath::~BehaviorFollowPath()
{

}

void BehaviorFollowPath::ownSetUp()
{
  std::cout << "ownSetup" << std::endl;

  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory,
                                 "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");

  private_nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "estimated_pose");
  private_nh.param<std::string>("controllers_topic", controllers_str, "command/high_level");
  private_nh.param<std::string>("rotation_angles_topic", rotation_angles_str, "rotation_angles");
  private_nh.param<std::string>("estimated_speed_topic",estimated_speed_str,"estimated_speed");
  private_nh.param<std::string>("yaw_controller_str",yaw_controller_str , "droneControllerYawRefCommand");
  private_nh.param<std::string>("service_topic_str",service_topic_str , "droneTrajectoryController/setControlMode");
  private_nh.param<std::string>("mission_point",mission_point, "droneMissionPoint");
  private_nh.param<std::string>("speed_reset", speed_reset_str,"droneSpeedsRefs");
  private_nh.param<std::string>("droneTrajectoryAbsRefCommand_topic", trajectory_ref_command_str, "droneTrajectoryAbsRefCommand");
  private_nh.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
  private_nh.param<std::string>("society_pose_topic",society_pose_str,"societyPose");
  private_nh.param<std::string>("obstacles_topic",obstacles_str,"obstacles");
}

void BehaviorFollowPath::ownStart()
{
  std::cout << "ownStart" << std::endl;
  is_finished = false;

  /*Initialize topics*/
  estimated_pose_sub = node_handle.subscribe(estimated_pose_str, 1000, &BehaviorFollowPath::estimatedPoseCallBack, this);
  rotation_angles_sub = node_handle.subscribe(rotation_angles_str, 1000, &BehaviorFollowPath::rotationAnglesCallback, this);
  estimated_speed_sub = node_handle.subscribe(estimated_speed_str, 1000, &BehaviorFollowPath::estimatedSpeedCallback, this);
  controllers_pub = node_handle.advertise<droneMsgsROS::droneCommand>(controllers_str, 1000, true);
  yaw_controller_pub=node_handle.advertise<droneMsgsROS::droneYawRefCommand>(yaw_controller_str,1000);
  mode_service = node_handle.serviceClient<droneMsgsROS::setControlMode>(service_topic_str);
  drone_speeds_reseter_pub = node_handle.advertise<droneMsgsROS::droneSpeeds>(speed_reset_str, 1);
  reference_trajectory_pub = node_handle.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>(trajectory_ref_command_str,100);
  query_client = node_handle.serviceClient <droneMsgsROS::ConsultBelief> (execute_query_srv);
  society_pose_sub=node_handle.subscribe(society_pose_str,1000,&BehaviorFollowPath::societyPose,this);
  obstacles_sub=node_handle.subscribe(obstacles_str,1000,&BehaviorFollowPath::obstaclesCallback,this);

  fail=false;
  estimated_pose_msg.x=0;
  estimated_pose_msg.y=0;
  estimated_pose_msg.z=0;
  estimated_pose_msg = *ros::topic::waitForMessage<droneMsgsROS::dronePose>(estimated_pose_str, node_handle);

  /*behavior implementation*/

  // Extract target position
  std::string arguments = getArguments();
  YAML::Node config_file = YAML::Load(arguments);
  droneMsgsROS::dronePositionTrajectoryRefCommand real_trajectory_msg;
  if(config_file["trajectory"].IsDefined()){
    std::vector<std::vector<double>> points=config_file["trajectory"].as<std::vector<std::vector<double>>>();

    for(int i=0;i<points.size();i++){
      droneMsgsROS::dronePositionRefCommand real_position_msg;
      real_trajectory_msg.header.stamp= ros::Time::now();
      real_position_msg.x = points[i][0];
      real_position_msg.y = points[i][1];
      real_position_msg.z = points[i][2];
      real_trajectory_msg.droneTrajectory.push_back(real_position_msg);
    }

  }
  else{
      setStarted(false);
      return;

  }
  std::cout << "The trajectory sent is " << target_position << std::endl;
  // Publish controller reference
  droneMsgsROS::dronePositionTrajectoryRefCommand reference_trajectory_msg;
  droneMsgsROS::dronePositionRefCommand reference_position_msg;
  reference_trajectory_msg.header.frame_id="GO_TO_POINT";
  reference_trajectory_msg.header.stamp= ros::Time::now();
  reference_position_msg.x = estimated_pose_msg.x;
  reference_position_msg.y = estimated_pose_msg.y;
  reference_position_msg.z = estimated_pose_msg.z;
  reference_trajectory_msg.droneTrajectory.push_back(reference_position_msg);
  reference_trajectory_pub.publish(reference_trajectory_msg);

  droneMsgsROS::droneYawRefCommand reference_yaw_msg;
  reference_yaw_msg.yaw = estimated_pose_msg.yaw;
  reference_yaw_msg.header.stamp = ros::Time::now();
  yaw_controller_pub.publish(reference_yaw_msg);

  // Wait for controller to change mode
  ros::topic::waitForMessage<droneMsgsROS::droneTrajectoryControllerControlMode>(
    "droneTrajectoryController/controlMode", node_handle
  );
  for(int i=0;i<10;i++)
  reference_trajectory_pub.publish(real_trajectory_msg);
  // Send target point

  std_msgs::Header header;
  header.frame_id = "behavior_go_to_point";

  droneMsgsROS::droneCommand msg;
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::MOVE;
  controllers_pub.publish(msg);
  static_pose.x=estimated_pose_msg.x;
  static_pose.y=estimated_pose_msg.y;
  static_pose.z=estimated_pose_msg.z;
  std::cout << "The trajectory sent is " << target_position << std::endl;
  // Publish controller reference
  reference_trajectory_msg.header.frame_id="GO_TO_POINT";
  reference_trajectory_msg.header.stamp= ros::Time::now();
  reference_position_msg.x = estimated_pose_msg.x;
  reference_position_msg.y = estimated_pose_msg.y;
  reference_position_msg.z = estimated_pose_msg.z;
  reference_trajectory_msg.droneTrajectory.push_back(reference_position_msg);
  reference_trajectory_pub.publish(reference_trajectory_msg);

  reference_yaw_msg.yaw = estimated_pose_msg.yaw;
  reference_yaw_msg.header.stamp = ros::Time::now();
  yaw_controller_pub.publish(reference_yaw_msg);

  // Wait for controller to change mode
  ros::topic::waitForMessage<droneMsgsROS::droneTrajectoryControllerControlMode>(
    "droneTrajectoryController/controlMode", node_handle
  );
  header.frame_id = "behavior_go_to_point";
  for(int i=0;i<10;i++)
  reference_trajectory_pub.publish(real_trajectory_msg);
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::MOVE;
  controllers_pub.publish(msg);
  static_pose.x=estimated_pose_msg.x;
  static_pose.y=estimated_pose_msg.y;
  static_pose.z=estimated_pose_msg.z;
}

void BehaviorFollowPath::ownRun()
{
  float pose_variation_maximum=0.2;
  float speed_maximum_at_end=0.1;
  if(is_finished==false)
  {
    if(std::abs(target_position.z-estimated_pose_msg.z)<pose_variation_maximum &&
    std::abs(target_position.x-estimated_pose_msg.x)<pose_variation_maximum &&
    std::abs(target_position.y- estimated_pose_msg.y)<pose_variation_maximum)
    {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
    if(timerIsFinished())
    {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
    if(fail)
    {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::WRONG_PROGRESS);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
    /*if(std::abs(target_position.z-estimated_pose_msg.z)<pose_variation_maximum &&
       std::abs(target_position.x-estimated_pose_msg.x)<pose_variation_maximum &&
       std::abs(target_position.y-estimated_pose_msg.y)<pose_variation_maximum &&
       estimated_speed_msg.dx>speed_maximum_at_end &&
       estimated_speed_msg.dz>speed_maximum_at_end &&
       estimated_speed_msg.dy>speed_maximum_at_end)
    {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::WRONG_PROGRESS);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }*/
  }


}
std::tuple<bool,std::string> BehaviorFollowPath::ownCheckActivationConditions()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador << "battery_level(self,LOW)";
  std::string query(capturador.str());
  query_service.request.query = query;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Battery low, unable to perform action");
    //return false;
  }
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,LANDED)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Drone landed");
    //return false;
  }

  return std::make_tuple(true,"");
  //return true;
}
void BehaviorFollowPath::ownStop()
{

  estimated_pose_sub.shutdown();
  rotation_angles_sub.shutdown();
  estimated_speed_sub.shutdown();
  //staticity_timer.stop();
}
void BehaviorFollowPath::estimatedSpeedCallback(const droneMsgsROS::droneSpeeds& msg)
{
  estimated_speed_msg=msg;
}
void BehaviorFollowPath::estimatedPoseCallBack(const droneMsgsROS::dronePose& msg)
{
  estimated_pose_msg=msg;
}
void BehaviorFollowPath::rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg)
{
  rotation_angles_msg=msg;
}
void BehaviorFollowPath::societyPose(const droneMsgsROS::societyPose& msg)
{
  double proximity_limit = 1;
  for(unsigned int i=0;i<msg.societyDrone.size();i++)
  {
      if((sqrt(pow((msg.societyDrone[i].pose.x- estimated_pose_msg.x),2)+
      pow((msg.societyDrone[i].pose.y-estimated_pose_msg.y),2)))< proximity_limit){
      fail=true;}
  }

}
void BehaviorFollowPath::obstaclesCallback(const droneMsgsROS::obstaclesTwoDim::ConstPtr& msg)
{
  fail=true;
}
