#ifndef GO_TO_POINT_H
#define GO_TO_POINT_H
#include <string>

// ROS
#include <ros/ros.h>
#include "std_srvs/Empty.h"
#include <droneMsgsROS/droneSpeeds.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <yaml-cpp/yaml.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/droneTrajectoryControllerControlMode.h>
#include <droneMsgsROS/setControlMode.h>
#include <droneMsgsROS/ConsultBelief.h>
#include <droneMsgsROS/obstaclesTwoDim.h>
#include <tuple>
// Aerostack msgs
#include <aerostack_msgs/BehaviorEvent.h>
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/droneCommand.h>
#include <droneMsgsROS/dronePitchRollCmd.h>
#include <droneMsgsROS/droneDAltitudeCmd.h>
#include <droneMsgsROS/droneDYawCmd.h>
#include<droneMsgsROS/societyPose.h>
//Aerostack libraries
#include <behavior_process.h>
#include <cmath>


class BehaviorFollowPath:public BehaviorProcess
{

public:
  BehaviorFollowPath();
  ~BehaviorFollowPath();
private:
  ros::NodeHandle node_handle;

  //Congfig variables
  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;
  std::string behavior_name_str;
  std::string estimated_pose_str;
  std::string rotation_angles_str;
  std::string controllers_str;
  std::string estimated_speed_str;
  std::string yaw_controller_str;
  std::string service_topic_str;
  std::string drone_position_str;
  std::string mission_point;
  std::string d_pitch_roll_str;
  std::string d_yaw_str;
  std::string d_altitude_str;
  std::string speed_reset_str;
  std::string trajectory_ref_command_str;
  std::string execute_query_srv;
  std::string society_pose_str;
  std::string obstacles_str;

  //Subscriber---
  ros::Subscriber estimated_pose_sub;
  ros::Subscriber estimated_speed_sub;
  ros::Subscriber rotation_angles_sub;
  ros::Publisher controllers_pub;
  ros::Publisher yaw_controller_pub;
  ros::Publisher  mission_point_pub;
  ros::ServiceClient mode_service;
  ros::ServiceClient query_client;
  ros::Publisher drone_speeds_reseter_pub;
  ros::Publisher reference_trajectory_pub;
  ros::Subscriber society_pose_sub;
  ros::Subscriber obstacles_sub;
  //Timer staticity_timer;

  //Message
  droneMsgsROS::dronePose estimated_pose_msg;
  droneMsgsROS::dronePose static_pose;
  droneMsgsROS::dronePose target_position;
  droneMsgsROS::droneSpeeds estimated_speed_msg;
  geometry_msgs::Vector3Stamped rotation_angles_msg;
  bool is_finished;
  bool fail=false;
  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();
  std::tuple<bool,std::string> ownCheckActivationConditions();
  void estimatedPoseCallBack(const droneMsgsROS::dronePose&);
  void estimatedSpeedCallback(const droneMsgsROS::droneSpeeds&);
  void rotationAnglesCallback(const geometry_msgs::Vector3Stamped&);
  void societyPose(const droneMsgsROS::societyPose&);
  void obstaclesCallback(const droneMsgsROS::obstaclesTwoDim::ConstPtr& msg);
};
#endif
