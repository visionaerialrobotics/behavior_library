/*!*******************************************************************************************
 *  \file       behavior_take_off.cpp
 *  \brief      Behavior TakeOff implementation file.
 *  \details    This file implements the BehaviorTakeOff class.
 *  \authors    Alberto Camporredondo
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/behavior_take_off.h"

BehaviorTakeOff::BehaviorTakeOff() : BehaviorProcess()
{

}

BehaviorTakeOff::~BehaviorTakeOff()
{

}


void BehaviorTakeOff::ownSetUp()
{
  std::cout << "ownSetup" << std::endl;

  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory,
                  "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");

  private_nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "estimated_pose");
  private_nh.param<std::string>("controllers_topic", controllers_str, "command/high_level");
  private_nh.param<std::string>("rotation_angles_topic", rotation_angles_str, "rotation_angles");
  private_nh.param<std::string>("initialize_yaw_srv", initialize_yaw_str, "droneOdometryStateEstimator/setInitDroneYaw");
  private_nh.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
  query_client = node_handle.serviceClient <droneMsgsROS::ConsultBelief> (execute_query_srv);
}

void BehaviorTakeOff::ownStart()
{
  std::cout << "ownStart" << std::endl;
  /*Initialize topics*/
  estimated_pose_sub = node_handle.subscribe(estimated_pose_str, 1000, &BehaviorTakeOff::estimatedPoseCallBack, this);
  rotation_angles_sub = node_handle.subscribe(rotation_angles_str, 1000, &BehaviorTakeOff::rotationAnglesCallback, this);
  controllers_pub = node_handle.advertise<droneMsgsROS::droneCommand>(controllers_str, 1, true);
  initialize_yaw_cli = node_handle.serviceClient<droneMsgsROS::setInitDroneYaw_srv_type>(initialize_yaw_str);


  /*behavior implementation*/
  std_msgs::Header header;
  header.frame_id = "behavior_take_off";

  droneMsgsROS::droneCommand msg;
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::TAKE_OFF;
  controllers_pub.publish(msg);
  estimated_pose_msg.z = 0;
  first_position = false;
  is_finished = false;
}

void BehaviorTakeOff::ownRun()
{
  if(!is_finished){
    monitor();
  }
}

//ownRun
void BehaviorTakeOff::ownStop()
{
  std::cout << "ownStop" << std::endl;
  std_msgs::Header header;
  header.frame_id = "behavior_land";

  droneMsgsROS::droneCommand msg;
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::HOVER;
  controllers_pub.publish(msg);

  estimated_pose_sub.shutdown();
  rotation_angles_sub.shutdown();
  controllers_pub.shutdown();
  initialize_yaw_cli.shutdown();
}

std::tuple<bool,std::string> BehaviorTakeOff::ownCheckActivationConditions()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador << "battery_level(self,LOW)";
  std::string query(capturador.str());
  query_service.request.query = query;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Battery low, unable to perform action");
    //return false;
  }
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,FLYING)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Already flying");
    //return false;
  }

  return std::make_tuple(true,"");
  //return true;
}

//Private functions
bool BehaviorTakeOff::monitor()
{
  double precision_take_off = 0.1;
  //  double precision_bad_pose = 1.0;

  //Check achievement
  if(std::abs(std::abs(estimated_pose_msg.z) - 0.7) < precision_take_off){ //TODO: mejorarr
    BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
    BehaviorProcess::setFinishConditionSatisfied(true);
    endingImplementation();
    is_finished = true;
    return true;
  }

  //Check timeout
  if(timerIsFinished()){
    BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
    BehaviorProcess::setFinishConditionSatisfied(true);
    endingImplementation();
    is_finished = true;
    return true;
  }
  /*if(staticity_timer.isFinished() && std::abs(static_pose.z-estimated_pose_msg.z)<precision_take_off)
  {
    BehaviorProcess::setFinishEvent(droneMsgsROS::behaviorEvent::BLOCKED);
    BehaviorProcess::setFinishConditionSatisfied(true);
    endingImplementation();
    return true;

  }
  if(estimated_pose_msg.z - 0.7 > precision_bad_pose)
  {
    BehaviorProcess::setFinishEvent(droneMsgsROS::behaviorEvent::WRONG_PROGRESS);
    BehaviorProcess::setFinishConditionSatisfied(true);
    endingImplementation();
    return true;

    }*/
  return false;
}

void BehaviorTakeOff::endingImplementation()
{
  droneMsgsROS::setInitDroneYaw_srv_type init_yaw_msg;
  init_yaw_msg.request.yaw_droneLMrT_telemetry_rad = (rotation_angles_msg.vector.z)*(M_PI/180.0);
  initialize_yaw_cli.call(init_yaw_msg); //TODO: informar de error si no se ha enviado correctamente

  droneMsgsROS::droneCommand msg;
  msg.command = droneMsgsROS::droneCommand::HOVER;
  controllers_pub.publish(msg);
}

//Custom topic Callbacks
void BehaviorTakeOff::estimatedPoseCallBack(const droneMsgsROS::dronePose& message){
  if(!first_position)
       static_pose = message;
  estimated_pose_msg = message;
}

void BehaviorTakeOff::rotationAnglesCallback(const geometry_msgs::Vector3Stamped& message){
  rotation_angles_msg = message;
}
