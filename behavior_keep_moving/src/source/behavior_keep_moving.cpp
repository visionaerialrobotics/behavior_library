/*!*******************************************************************************************
 *  \file       behavior_keep_moving.cpp
 *  \brief      Behavior Keep Moving implementation file.
 *  \details    This file implements the behaviorKeepMoving class.
 *  \authors    Rafael Artiñano Muñoz
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "behavior_keep_moving.h"





behaviorKeepMoving::behaviorKeepMoving():BehaviorProcess()
{

}

behaviorKeepMoving::~behaviorKeepMoving()
{

}

void behaviorKeepMoving::ownSetUp()
{
    std::cout << "ownSetup" << std::endl;

    node_handle.param<std::string>("drone_id", drone_id, "1");
    node_handle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
    node_handle.param<std::string>("my_stack_directory", my_stack_directory,
                                   "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");

    node_handle.param<std::string>("estimated_pose_topic", estimated_pose_str, "estimated_pose");
    node_handle.param<std::string>("controllers_topic", controllers_str, "command/high_level");
    node_handle.param<std::string>("rotation_angles_topic", rotation_angles_str, "rotation_angles");
    node_handle.param<std::string>("estimated_speed_topic",estimated_speed_str,"estimated_speed");
    node_handle.param<std::string>("yaw_controller_str",yaw_controller_str , "droneControllerYawRefCommand");
    node_handle.param<std::string>("service_topic_str",service_topic_str , "droneTrajectoryController/setControlMode");
    node_handle.param<std::string>("drone_position_str",drone_position_str , "dronePositionRefs");
    node_handle.param<std::string>("speed_topic",speed_topic , "droneSpeedsRefs");
    node_handle.param<std::string>("drone_control_mode",drone_control_mode_str,"droneTrajectoryController/controlMode");
    node_handle.param<std::string>("d_altitude",d_altitude_str,"command/dAltitude");
    node_handle.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
}

void behaviorKeepMoving::ownStart()
{
    std::cout << "ownStart" << std::endl;
    /*Initialize topics*/
    estimated_pose_sub = node_handle.subscribe(estimated_pose_str, 1000, &behaviorKeepMoving::estimatedPoseCallBack, this);
    rotation_angles_sub = node_handle.subscribe(rotation_angles_str, 1000, &behaviorKeepMoving::rotationAnglesCallback, this);
    estimated_speed_sub = node_handle.subscribe(estimated_speed_str, 1000, &behaviorKeepMoving::estimatedSpeedCallback, this);
    controllers_pub = node_handle.advertise<droneMsgsROS::droneCommand>(controllers_str, 1, true);
    yaw_controller_pub=node_handle.advertise<droneMsgsROS::droneYawRefCommand>(yaw_controller_str,1000);
    mode_service=node_handle.serviceClient<droneMsgsROS::setControlMode>(service_topic_str);
    drone_position_pub=node_handle.advertise< droneMsgsROS::dronePositionRefCommandStamped>(drone_position_str,1000);
    speed_topic_pub=node_handle.advertise<droneMsgsROS::droneSpeeds>(speed_topic,1000);
    d_altitude_pub = node_handle.advertise<droneMsgsROS::droneDAltitudeCmd>(d_altitude_str,1);
    query_client = node_handle.serviceClient <droneMsgsROS::ConsultBelief> (execute_query_srv);

    /*behavior implementation*/
    std::string arguments=getArguments();
    YAML::Node config_file = YAML::Load(arguments);

    std::string direction = config_file["direction"].as<std::string>();
    double speed = config_file["speed"].as<double>();
    if(direction == "UP")
    {
      target_position.dx=0;
      target_position.dy=0;
      target_position.dz=speed;

    }
    else if(direction == "DOWN")
    {
      target_position.dx=0;
      target_position.dy=0;
      target_position.dz=-speed;

    }
    else if(direction == "LEFT")
    {
      target_position.dx=-speed;
      target_position.dy=0;
      target_position.dz=0;

    }
    else if(direction == "RIGHT")
    {
      target_position.dx=speed;
      target_position.dy=0;
      target_position.dz=0;

    }
    else if(direction == "FORWARD")
    {
      target_position.dx=0;
      target_position.dy=speed;
      target_position.dz=0;
    }
    else if(direction == "BACKWARD")
    {
      target_position.dx=0;
      target_position.dy=-speed;
      target_position.dz=0;

    }
    else
    {
      std::cout<<"argument given is invalid"<< std::endl;
      setStarted(false);
      return;
    }

    estimated_speed_msg = *ros::topic::waitForMessage<droneMsgsROS::droneSpeeds>(estimated_speed_str, node_handle, ros::Duration(2));
    estimated_pose_msg = *ros::topic::waitForMessage<droneMsgsROS::dronePose>(estimated_pose_str, node_handle, ros::Duration(2));

    droneMsgsROS::setControlMode mode;
    mode.request.controlMode.command=mode.request.controlMode.SPEED_CONTROL;
    mode_service.call(mode);

    droneMsgsROS::droneSpeeds point;
    point.dx=estimated_speed_msg.dx;
    point.dy=estimated_speed_msg.dy;
    point.dz=estimated_speed_msg.dz;
    speed_topic_pub.publish(point);

    ros::topic::waitForMessage<droneMsgsROS::droneTrajectoryControllerControlMode>(
      drone_control_mode_str, node_handle
    );
      point.dx=target_position.dx;
      point.dy=target_position.dy;
      point.dz=target_position.dz;
      speed_topic_pub.publish(point);

      droneMsgsROS::droneCommand msg;
      msg.command = droneMsgsROS::droneCommand::MOVE;
      controllers_pub.publish(msg);

      estimated_speed_msg = *ros::topic::waitForMessage<droneMsgsROS::droneSpeeds>(estimated_speed_str, node_handle, ros::Duration(2));

}

void behaviorKeepMoving::ownRun()
{


}
std::tuple<bool,std::string> behaviorKeepMoving::ownCheckActivationConditions()
{
    droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador << "battery_level(self,LOW)";
  std::string query(capturador.str());
  query_service.request.query = query;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Battery low, unable to perform action");
    //return false;
  }
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,LANDED)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Drone landed");
    //return false;
  }

  return std::make_tuple(true,"");
}
void behaviorKeepMoving::ownStop()
{
    // droneMsgsROS::droneCommand msg;
    // msg.command = droneMsgsROS::droneCommand::HOVER;
    // controllers_pub.publish(msg);

    is_finished = false;

    estimated_pose_sub.shutdown();
    rotation_angles_sub.shutdown();
    estimated_speed_sub.shutdown();

}
void behaviorKeepMoving::estimatedSpeedCallback(const droneMsgsROS::droneSpeeds& msg)
{
    estimated_speed_msg=msg;

}
void behaviorKeepMoving::estimatedPoseCallBack(const droneMsgsROS::dronePose& msg)
{
    estimated_pose_msg=msg;
}
void behaviorKeepMoving::rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg)
{
    rotation_angles_msg=msg;
}
