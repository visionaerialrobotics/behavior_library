/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include <aerostack_msgs/StartBehavior.h>
#include <behavior_inform_operator.h>
#include <cstdio>
#include <gtest/gtest.h>
#include <iostream>
#include <string>
#include <thread>

ros::NodeHandle *nh;

TEST(BehaviorInformOperatorTests, simpleInformOperatorTest)
{
  std::cout << "START TEST" << std::endl;

  aerostack_msgs::StartBehavior::Request msg;
  aerostack_msgs::StartBehavior::Response res;
  std::string behavior_path = "/drone1/behavior_inform_operator/start";
  ros::ServiceClient behavior_cli = nh->serviceClient<aerostack_msgs::StartBehavior>(behavior_path);
  msg.arguments = "message: \"Sensors detect a low level of light intensity in the environment where the aerial robot is flying.\"";
  behavior_cli.call(msg, res);
  ros::Rate rate(30);

  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();
  rate.sleep();
  ros::spinOnce();

  std::string response;
  std::cout << "\033[1;34m Has a window with the message:n 'The battery is low' appeared? (Y/N) \033[0m" << std::endl;
  getline(std::cin, response);

  EXPECT_TRUE(response == "Y");
}

/*--------------------------------------------*/
/*  Main  */

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  nh = new ros::NodeHandle;
  std::cout << "WAIT WHILE LAUNCHING AEROSTACK" << std::endl;
  system("bash "
         "$AEROSTACK_STACK/stack/executive_system/behavior_management_system/behavior_library/behavior_inform_operator/"
         "src/controller/test/test.sh");
  ros::Duration(40).sleep();

  return RUN_ALL_TESTS();
}
