/*!*******************************************************************************************
 *  \file       behavior_keep_hovering.cpp
 *  \brief      Behavior Keep Hovering implementation file.
 *  \details    This file implements the BehaviorKeepHovering class.
 *  \authors    Rafael Artiñano Muñoz
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/behavior_keep_hovering.h"
BehaviorKeepHovering::BehaviorKeepHovering():BehaviorProcess()
{

}

BehaviorKeepHovering::~BehaviorKeepHovering()
{

}

void BehaviorKeepHovering::ownSetUp()
{
  timer_msg=false;
  std::cout << "ownSetup" << std::endl;

  node_handle.param<std::string>("drone_id", drone_id, "1");
  node_handle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  node_handle.param<std::string>("my_stack_directory", my_stack_directory,
                  "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");


  node_handle.param<std::string>("estimated_pose_topic", estimated_pose_str, "estimated_pose");
  node_handle.param<std::string>("controllers_topic", controllers_str, "command/high_level");
  node_handle.param<std::string>("speed_reset", speed_reset_str,"droneSpeedsRefs");
  node_handle.param<std::string>("drone_pitch_roll",drone_pitch_roll_str,"command/pitch_roll");
  node_handle.param<std::string>("d_yaw",d_yaw_str,"command/dYaw");
  node_handle.param<std::string>("d_altitude",d_altitude_str,"command/dAltitude");
  node_handle.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
 
  query_client = node_handle.serviceClient <droneMsgsROS::ConsultBelief> (execute_query_srv);

}

void BehaviorKeepHovering::ownStart()
{
  timer_msg=false;
  std::cout << "ownStart" << std::endl;
  is_finished = false;
  marker_end=false;
  timeout_end=false;
  marker_found=false;
  /*Initialize topics*/
  controllers_pub = node_handle.advertise<droneMsgsROS::droneCommand>(controllers_str, 1000, true);
  estimated_pose_sub = node_handle.subscribe(estimated_pose_str, 1000, &BehaviorKeepHovering::estimatedPoseCallBack, this);
  drone_speeds_reseter_pub = node_handle.advertise<droneMsgsROS::droneSpeeds>(speed_reset_str, 1);
  d_pitch_roll_pub = node_handle.advertise<droneMsgsROS::dronePitchRollCmd>(drone_pitch_roll_str,1);
  d_yaw_pub = node_handle.advertise<droneMsgsROS::droneDYawCmd>(d_yaw_str,1);
  d_altitude_pub = node_handle.advertise<droneMsgsROS::droneDAltitudeCmd>(d_altitude_str,1);




  /*behavior implementation*/
  droneMsgsROS::dronePitchRollCmd pitch_roll_msg;
  droneMsgsROS::droneDAltitudeCmd d_altitude_msg;
  droneMsgsROS::droneDYawCmd d_yaw_msg;
  droneMsgsROS::droneSpeeds reset;
  reset.dx=0.0;
  reset.dy=0.0;
  reset.dz=0.0;
  reset.dyaw=0.0;
  reset.dpitch=0.0;
  reset.droll=0.0;
  pitch_roll_msg.pitchCmd = 0.0;
  pitch_roll_msg.rollCmd = 0.0;
  d_altitude_msg.dAltitudeCmd = 0.0;
  d_yaw_msg.dYawCmd = 0.0;

  d_altitude_pub.publish(d_altitude_msg);
  d_yaw_pub.publish(d_yaw_msg);
  d_pitch_roll_pub.publish(pitch_roll_msg);
  drone_speeds_reseter_pub.publish(reset);
  std_msgs::Header header;
  header.frame_id = "behavior_go_to_point";
  droneMsgsROS::droneCommand msg;
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::HOVER;
  controllers_pub.publish(msg);
  //staticity_timer.time(5);
  estimated_pose_msg = *ros::topic::waitForMessage<droneMsgsROS::dronePose>(estimated_pose_str, node_handle);
  static_pose.x=estimated_pose_msg.x;
  static_pose.y=estimated_pose_msg.y;
  static_pose.z=estimated_pose_msg.z;
}

void BehaviorKeepHovering::ownRun()
{


}
std::tuple<bool, std::string> BehaviorKeepHovering::ownCheckActivationConditions()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador << "battery_level(self,LOW)";
  std::string query(capturador.str());
  query_service.request.query = query;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Battery low, unable to perform action");
    //return false;
  }
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,FLYING)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(!query_service.response.success)
  { 
    return std::make_tuple(false,"Error: Drone landed");
    //return false;
  }

  return std::make_tuple(true,"");
  //return true;
}
void BehaviorKeepHovering::ownStop()
{
  // droneMsgsROS::dronePitchRollCmd pitch_roll_msg;
  // droneMsgsROS::droneDAltitudeCmd d_altitude_msg;
  // droneMsgsROS::droneDYawCmd d_yaw_msg;
  // droneMsgsROS::droneSpeeds reset;
  // reset.dx=0.0;
  // reset.dy=0.0;
  // reset.dz=0.0;
  // reset.dyaw=0.0;
  // reset.dpitch=0.0;
  // reset.droll=0.0;
  // pitch_roll_msg.pitchCmd = 0.0;
  // pitch_roll_msg.rollCmd = 0.0;
  // d_altitude_msg.dAltitudeCmd = 0.0;
  // d_yaw_msg.dYawCmd = 0.0;
  //
  // d_altitude_pub.publish(d_altitude_msg);
  // d_yaw_pub.publish(d_yaw_msg);
  // d_pitch_roll_pub.publish(pitch_roll_msg);
  // drone_speeds_reseter_pub.publish(reset)timer_msg
  //
  // droneMsgsROS::droneCommand msg;
  // msg.header = header;
  // msg.command = droneMsgsROS::droneCommand::HOVER;
  // controllers_pub.publish(msg);
  usleep(1000000);


  //staticity_timer.stop();
}
void BehaviorKeepHovering::estimatedPoseCallBack(const droneMsgsROS::dronePose& msg)
{
estimated_pose_msg=msg;
}
