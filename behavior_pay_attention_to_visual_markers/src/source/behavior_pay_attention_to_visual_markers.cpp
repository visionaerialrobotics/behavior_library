/*!*******************************************************************************************
 *  \file       behavior_pay_attention_to_visual_markers.cpp
 *  \brief      Behavior Pay Attention To Visual Markers implementation file.
 *  \details    This file implements the BehaviorPayAttentionToVisualMarkers class.
 *  \authors    Rafael Artiñano Muñoz
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/behavior_pay_attention_to_visual_markers.h"
BehaviorPayAttentionToVisualMarkers::BehaviorPayAttentionToVisualMarkers():BehaviorProcess()
{

}

BehaviorPayAttentionToVisualMarkers::~BehaviorPayAttentionToVisualMarkers()
{

}

void BehaviorPayAttentionToVisualMarkers::ownSetUp()
{
  std::cout << "ownSetup" << std::endl;

  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory,
                                 "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");


}

void BehaviorPayAttentionToVisualMarkers::ownStart()
{
  std::cout << "ownStart" << std::endl;

  /*Initialize topics*/
}

void BehaviorPayAttentionToVisualMarkers::ownRun()
{

}
std::tuple<bool,std::string> BehaviorPayAttentionToVisualMarkers::ownCheckActivationConditions()
{
  return std::make_tuple(true,"");
}
void BehaviorPayAttentionToVisualMarkers::ownStop()
{

}
