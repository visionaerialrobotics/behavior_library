# follow_path_with_trajectory_controller

**Purpose**: This behavior follows a path while using the trajectory controller. This behavior does not finish until the robot reach the last point. The argument is an tuple of coordinates.

**Type of behavior:** Goal-based behavior.

**Arguments:** 

| Name    |   Format  |  Example |  
| :-----------| :---------| :--------|
| path: |Tuple of coordinates x, y, z (meters)|path: [[1.23, 2, 0.5],[2.5,3,1],[3.1,3.4,1.5]]|	

# Diagram

![Screenshot 2019-05-21 12:47:02.png](https://bitbucket.org/repo/rokr9B/images/1816296653-Screenshot%202019-05-21%2012:47:02.png)
----
# Contributors

**Code maintainer:** Ra�l Cruz

**Authors:** Ra�l Cruz
