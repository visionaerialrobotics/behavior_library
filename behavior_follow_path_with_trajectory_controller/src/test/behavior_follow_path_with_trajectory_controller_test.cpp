/*!*******************************************************************************************
 *  \file       behavior_follow_path_with_trajectory_controller_test.cpp
 *  \brief      Behavior Follow Path With Trajectory Controller implementation file.
 *  \details    This file implements the behaviorFollowPathWithTrajectoryController test.
 *  \authors    Raul Cruz
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include <gtest/gtest.h>
#include <string>

#include <queue>
#include <thread>
// ROS
#include <ros/ros.h>
#include "std_srvs/Empty.h"
#include <droneMsgsROS/droneSpeeds.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <yaml-cpp/yaml.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/droneTrajectoryControllerControlMode.h>
#include <droneMsgsROS/setControlMode.h>
#include <droneMsgsROS/ConsultBelief.h>
#include <tuple>
// Aerostack msgs
#include <aerostack_msgs/RequestBehavior.h>
#include <aerostack_msgs/BehaviorCommand.h>
#include <aerostack_msgs/BehaviorEvent.h>
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/droneCommand.h>
#include <droneMsgsROS/dronePitchRollCmd.h>
#include <droneMsgsROS/droneDAltitudeCmd.h>
#include <droneMsgsROS/droneDYawCmd.h>
#include <droneMsgsROS/societyPose.h>
//Aerostack libraries
#include <behavior_process.h>
#include <cmath>

/*

 If you want to prove this test, you must launch the launcherTest and after this, launch the test which is located in /devel/lib/behavior_follow_path_with_trajectory_controller
 This test proves that follow_path_with_trajectory_controller works correctly by looking to the topics where it publish.

 First of all, we call tho this behavior with a path. Also, we have a callback with the localization of the drone.
 In this callback it is tested that the drone flies through certains positions from the path.
 In this case the path is (1,1,1),(1,1.5,0.5),(2,3,0.5)
*/

bool finished=false;
bool publish = false;
bool publish2 = false;
bool publish3 = false;

int passTest1=0;
int passTest2=0;
void spinnerThread(){
    while(!finished){
        ros::spinOnce();
    }

}
void chatterCallback(const droneMsgsROS::dronePose &resp_path)
{
  
  if( resp_path.x < 2.5  and resp_path.x > 1.5){ publish2=true; }
  if( resp_path.y < 3.5  and resp_path.y > 2.5){ publish=true; passTest1=0;}
  if( resp_path.z < 1  and resp_path.z > 0.2){ publish3=true; }
  if( resp_path.x < 1.5  and resp_path.x > 0.5){ publish2=true; }
  if( resp_path.y < 2  and resp_path.y > 1){ publish=true; }
  if( resp_path.z < 1  and resp_path.z > 0.2){ publish3=true; }
  if( resp_path.x < 1.5  and resp_path.x > 0.5){ publish2=true; }
  if( resp_path.y < 1.5  and resp_path.y > 0.5){ publish=true; }
  if( resp_path.z < 1  and resp_path.z > 0.2){ publish3=true; }
  
  
}
TEST(BehaviorFollowPathWithTrajectoryControllerTests, topicTest)
{
publish = false;
publish2 = false;
publish3 = false;
passTest1=1;
ros::NodeHandle node_handle;   
ros::Subscriber path_sub;
std::cout <<"Test1 Path:(1,1,1),(1,1.5,0.5),(2,3,0.5)"<<std::endl;
ros::ServiceClient path_client;   
path_client = node_handle.serviceClient<aerostack_msgs::RequestBehavior>("/drone11/activate_behavior");
path_sub = node_handle.subscribe("/drone11/EstimatedPose_droneGMR_wrt_GFF", 1000, chatterCallback);
aerostack_msgs::RequestBehavior bahevior_follow_path;
aerostack_msgs::BehaviorCommand follow_path;
aerostack_msgs::RequestBehavior bahevior_take_off;
aerostack_msgs::BehaviorCommand take_off;
take_off.name="TAKE_OFF";
bahevior_take_off.request.behavior=take_off;
path_client.call(bahevior_take_off);
follow_path.name="FOLLOW_PATH_WITH_TRAJECTORY_CONTROLLER";
follow_path.arguments="path: [[1,1,0.5], [1,1.5,0.5], [2,3,0.5]]";
bahevior_follow_path.request.behavior=follow_path;
path_client.call(bahevior_follow_path);
std::cout <<"Waiting to EstimatedPose_droneGMR_wrt_GFF"<<std::endl;
while (passTest1!=0){}
passTest1=1;
EXPECT_TRUE(publish and publish2 and publish3);
path_sub.shutdown();
}
/*
 This test proves that follow_path_with_trajectory_controller works correctly by looking to the topics where it publish.

 First of all, we call tho this behavior with a path. Also, we have a callback with the localization of the drone.
 In this callback it is tested that the drone flies through certains positions from the path.
 In this case the path is (2,1,1),(2,1.5,0.5),(3,5,0.5)
*/
void chatterCallback2(const droneMsgsROS::dronePose &resp_path)
{
  
  if( resp_path.x < 3.5  and resp_path.x > 2.5){ publish2=true; }
  if( resp_path.y < 5.5  and resp_path.y > 4.5){ publish=true; passTest2=0;}
  if( resp_path.z < 1  and resp_path.z > 0.2){ publish3=true; }
  if( resp_path.x < 2.5  and resp_path.x > 1.5){ publish2=true; }
  if( resp_path.y < 2  and resp_path.y > 1){ publish=true; }
  if( resp_path.z < 1  and resp_path.z > 0.2){ publish3=true; }
  if( resp_path.x < 2.5  and resp_path.x > 1.5){ publish2=true; }
  if( resp_path.y < 1.5  and resp_path.y > 0.5){ publish=true; }
  if( resp_path.z < 1  and resp_path.z > 0.2){ publish3=true; }
  
}
TEST(BehaviorFollowPathWithOccupancyGridTests, topicTest2)
{
publish = false;
publish2 = false;
publish3 = false;
passTest2=1;
//waiting a little bit for finishing correctly the previous follow path
system("sleep 5");
ros::NodeHandle node_handle;   
ros::Subscriber path_sub;
std::cout <<"Test2 Path:(2,1,1),(2,1.5,0.5),(3,5,0.5)"<<std::endl;
ros::ServiceClient path_client;    
system("rosservice call /drone11/behavior_follow_path_with_trajectory_controller/stop");
system("sleep 5");
path_client = node_handle.serviceClient<aerostack_msgs::RequestBehavior>("/drone11/activate_behavior");
path_sub = node_handle.subscribe("/drone11/EstimatedPose_droneGMR_wrt_GFF", 1000, chatterCallback2);
aerostack_msgs::RequestBehavior bahevior_follow_path;
aerostack_msgs::BehaviorCommand follow_path;
follow_path.name="FOLLOW_PATH_WITH_TRAJECTORY_CONTROLLER";
follow_path.arguments="path: [[2,1,0.5], [2,1.5,0.5], [3,5,0.5]]";
bahevior_follow_path.request.behavior=follow_path;
path_client.call(bahevior_follow_path);
std::cout <<"Waiting to EstimatedPose_droneGMR_wrt_GFF"<<std::endl;
while (passTest2!=0){}

EXPECT_TRUE(publish);
path_sub.shutdown();
}  

int main(int argc, char** argv){
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, ros::this_node::getName());
    std::thread thr(&spinnerThread);
    system("rosservice call /drone11/droneRobotLocalizationROSModuleNode/start");
    return RUN_ALL_TESTS();


}

