#!/bin/bash

#---------------------------------------------------------------------------------------------
# INTERNAL PROCESSES
#---------------------------------------------------------------------------------------------
xfce4-terminal  \
`#---------------------------------------------------------------------------------------------` \
`# Quadrotor simulator                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Simulator"   --command "bash -c \"
roslaunch droneSimulatorROSModule droneSimulatorROSModule.launch --wait \
    drone_id_namespace:=drone11 \
    drone_id_int:=11 \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
--tab --title "Midlevel Controller" --command "bash -c \"
roslaunch droneMidLevelAutopilotROSModule droneMidLevelAutopilotROSModule.launch --wait drone_id_namespace:=drone11 drone_id_int:=11 my_stack_directory:=${AEROSTACK_STACK};
                        exec bash\""  \
    --tab --title "RobotLocalizationROSModule" --command "bash -c \"
roslaunch droneRobotLocalizationROSModule droneRobotLocalizationROSModule.launch --wait drone_id_namespace:=drone11 drone_id_int:=11 my_stack_directory:=${AEROSTACK_STACK};
                        exec bash\""  \
  --tab --title "Robot localization" --command "bash -c \"
roslaunch ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/launch_files/Ekf.launch --wait drone_id_namespace:=drone11 drone_id_int:=11 my_stack_directory:=${AEROSTACK_STACK};
                        exec bash\""  \
    --tab --title "Drone Trajectory Controller" --command "bash -c \"
roslaunch droneTrajectoryControllerROSModule droneTrajectoryControllerROSModule.launch --wait \
      drone_id_namespace:=drone11 drone_id_int:=11 my_stack_directory:=${AEROSTACK_STACK} \
      drone_estimated_pose_topic_name:=EstimatedPose_droneGMR_wrt_GFF drone_estimated_speeds_topic_name:=EstimatedSpeed_droneGMR_wrt_GFF;
    exec bash\""  \
    --tab --title "DroneSupervisor" --command "bash -c \"
roslaunch  process_monitor_process process_monitor.launch --wait drone_id_namespace:=drone11 drone_id_int:=11 my_stack_directory:=${AEROSTACK_STACK};
                        exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Communication Manager                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "DroneCommunicationManager" --command "bash -c \"
roslaunch droneCommunicationManagerROSModule droneCommunicationManagerROSModule.launch --wait \
  drone_id_namespace:=drone11 \
  drone_id_int:=11 \
  my_stack_directory:=${AEROSTACK_STACK} \
  estimated_pose_topic_name:=estimated_pose;
                        exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Trajectory planner                                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Trajectory Planner" --command "bash -c \"
roslaunch droneTrajectoryPlannerROSModule droneTrajectoryPlanner2dROSModule.launch --wait \
    drone_id_namespace:=drone11 \
    drone_id_int:=11 \
    my_stack_directory:=${AEROSTACK_STACK} \
    drone_pose_topic_name:=estimated_pose;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Yaw commander                                                                               ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Yaw Commander" --command "bash -c \"
roslaunch droneYawCommanderROSModule droneYawCommanderROSModule.launch --wait \
    drone_id_namespace:=drone11 \
    drone_id_int:=11 \
    my_stack_directory:=${AEROSTACK_STACK} \
    drone_pose_topic_name:=estimated_pose;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Process manager                                                                        ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Process manager" --command "bash -c \"
roslaunch process_manager_process process_manager_process.launch --wait \
    drone_id_namespace:=drone11 \
    drone_id:=11 \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Resource Manager                                                                            ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Resource Manager" --command "bash -c \"
roslaunch resource_manager_process resource_manager_process.launch --wait \
    drone_id_namespace:=drone11 \
    drone_id:=11 \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
`#------------------------------------------------------------------------------` \
`# Belief Manager                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Manager" --command "bash -c \"
roslaunch belief_manager_process belief_manager_process.launch --wait \
    drone_id_namespace:=drone11 \
    drone_id:=11 \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Updater                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Updater" --command "bash -c \"
roslaunch belief_updater_process belief_updater_process.launch --wait \
    drone_id_namespace:=drone11 \
    drone_id:=11 \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  &

xfce4-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Behaviors                                                                                   ` \
`#---------------------------------------------------------------------------------------------` \
`#---------------------------------------------------------------------------------------------` \
`# Behavior TakeOff                                                                            ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior TakeOff" --command "bash -c \"
roslaunch behavior_take_off behavior_take_off.launch --wait \
    drone_id_namespace:=drone11 \
    drone_id:=11 \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Land                                                                               ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Land" --command "bash -c \"
roslaunch behavior_land behavior_land.launch --wait \
    drone_id_namespace:=drone11 \
    drone_id:=11 \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Behavior behavior_follow_path_with_trajectory_controller                                                                            ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Behavior Follow_ Path With Trajectory Controller" --command "bash -c \"
roslaunch behavior_follow_path_with_trajectory_controller behavior_follow_path_with_trajectory_controller.launch --wait \
    drone_id_namespace:=drone11 \
    drone_id:=11 \
    estimated_pose_topic:=EstimatedPose_droneGMR_wrt_GFF \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\"" \
--tab --title "Behavior Coordinator" --command "bash -c \"
roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
    drone_id_namespace:=drone11 \
    behavior_catalog_path:=${AEROSTACK_STACK}/configs/general/behavior_catalog.yaml \
    drone_id:=11 \
    my_stack_directory:=${AEROSTACK_STACK};
exec bash\""  &
