# behavior_pay_attention_to_robot_messages

**Purpose**: This behavior pays attention to messages sent by other robots. When the name of the message's receiver is the name of the own robot, the message is added to the belief memory. Each message must be written as a logic predicate according to the format of the belief memory.

**Type of behavior:** Recurrent.

**Implementation notes** 

The message is received by subscribing the following ROS topic:

- **social_communication_channel** ([aerostack_msgs/SocialCommunicationStatement](https://bitbucket.org/visionaerialrobotics/aerostack_msgs/src/master/msg/SocialCommunicationStatement.msg))	

# Contributors

**Code maintainer:** Abraham Carrera Groba

**Authors:** Abraham Carrera Groba