/*!*********************************************************************************
 *  \file       behavior_pay_attention_to_robot_messages.h
 *  \brief      BehaviorPayAttentionToRobotMessages definition file.
 *  \details    This file contains the BehaviorPayAttentionToRobotMessages declaration. To obtain more information about
 *              it's definition consult the behavior_pay_attention_to_robot_messages.cpp file.
 *  \authors    Abraham Carrera Groba.
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef BEHAVIOR_PAY_ATTENTION_TO_ROBOT_MESSAGES_H
#define BEHAVIOR_PAY_ATTENTION_TO_ROBOT_MESSAGES_H

// System
#include <iostream>
#include <map>
#include <signal.h>
#include <string>
#include <tuple>

// ROS
#include <ros/ros.h>

// Aerostack libraries
#include <behavior_process.h>

// Msgs
#include <aerostack_msgs/BehaviorEvent.h>
#include <aerostack_msgs/SocialCommunicationStatement.h>
#include <aerostack_msgs/AddBelief.h>

class BehaviorPayAttentionToRobotMessages : public BehaviorProcess
{
public:
  struct Behavior
  {
    std::string name;
    std::string arguments;
  };

public:
  BehaviorPayAttentionToRobotMessages();
  ~BehaviorPayAttentionToRobotMessages();

private:
  ros::NodeHandle node_handle;

  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;
  std::string social_communication_channel_str;
  std::string add_belief_str;

  aerostack_msgs::AddBelief::Request add_belief_msg;

  ros::ServiceClient add_belief_srv;
  ros::Subscriber social_communication_channel_sub;

  // BehaviorProcess
private:
  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();
  std::tuple<bool, std::string> ownCheckActivationConditions();

  // Callbacks
  void socialCommunicationChannelCallback(const aerostack_msgs::SocialCommunicationStatement &message);
};

#endif
