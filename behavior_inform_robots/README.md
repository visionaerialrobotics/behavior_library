# behavior_inform_robots

**Purpose**: This behavior sends a text message to another robot. The other robot must have activated the behavior PAY_ATTENTION_TO_ROBOT_MESSAGES. The text message must be a logic predicate according to the format of the belief memory.

**Type of behavior:** Goal-based behavior.

**Arguments:** 

| Name    |   Format  |  Example |  
| :-----------| :---------| :--------|
| source |String with the name of the sender | source: "drone1"|
| destination |String with the name of the receiver| destination: "drone2"|
| text | String with the text of the message as a logic predicate | text: "found(item07)"|

**Implementation notes** 

The message is sent to other robots by publishing the information in the following ROS topic:

- **social_communication_channel** ([aerostack_msgs/SocialCommunicationStatement](https://bitbucket.org/visionaerialrobotics/aerostack_msgs/src/master/msg/SocialCommunicationStatement.msg))
	
# Contributors

**Code maintainer:** Abraham Carrera Groba

**Authors:** Abraham Carrera Groba
