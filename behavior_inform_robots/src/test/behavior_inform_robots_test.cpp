/*!*******************************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include <aerostack_msgs/SocialCommunicationStatement.h>
#include <behavior_inform_robots.h>
#include <cstdio>
#include <gtest/gtest.h>
#include <iostream>
#include <string>
#include <thread>

ros::NodeHandle *nh;

class SocialCommunicationStatementsListener
{
public:
  ros::Subscriber social_communication_channel_sub;

  bool social_communication_statement_received;

  SocialCommunicationStatementsListener() { social_communication_statement_received = false; }

  void start()
  {
    social_communication_channel_sub =
        nh->subscribe("/drone1/social_communication_channel", 1,
                      &SocialCommunicationStatementsListener::socialCommunicationChannelCallback, this);
  }

  void socialCommunicationChannelCallback(const aerostack_msgs::SocialCommunicationStatement &message)
  {
    std::string text = message.text;
    std::string expected_text = "light_intensity(high)";
    if (text == expected_text)
    {
      social_communication_statement_received = true;
    }
  }
};

TEST(BehaviorInformRobotsTests, simpleInformRobotsTest)
{
  std::cout << "START TEST" << std::endl;

  SocialCommunicationStatementsListener scsl;
  scsl.start();

  ros::Duration(2).sleep();
  ros::Rate rate(30);

  aerostack_msgs::StartBehavior::Request msg;
  aerostack_msgs::StartBehavior::Response res;

  std::string behavior_path = "/drone1/behavior_inform_robots/start";

  ros::ServiceClient behavior_cli = nh->serviceClient<aerostack_msgs::StartBehavior>(behavior_path);

  msg.arguments = "source: \"drone1\"\ndestination: \"drone2\"\ntext: \"light_intensity(Adequate)\"";

  behavior_cli.call(msg, res);

  int it = 0;

  while (!scsl.social_communication_statement_received && it < 20)
  {
    ros::spinOnce();
    rate.sleep();
    it++;
  }

  EXPECT_TRUE(scsl.social_communication_statement_received);
}

/*--------------------------------------------*/
/*  Main  */

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  nh = new ros::NodeHandle;
  std::cout << "WAIT WHILE LAUNCHING AEROSTACK" << std::endl;
  system("bash "
         "$AEROSTACK_STACK/stack/executive_system/behavior_management_system/behavior_library/behavior_inform_robots/"
         "src/test/test.sh");
  ros::Duration(40).sleep();

  return RUN_ALL_TESTS();
}
