cmake_minimum_required(VERSION 2.8.3)
project(behavior_inform_robots)

### Use version 2011 of C++ (c++11). By default ROS uses c++98
add_definitions(-std=c++11)
add_definitions(-g)

# Directories definition
set(BEHAVIOR_INFORM_ROBOTS_SOURCE_DIR
  src/source
)

set(BEHAVIOR_INFORM_ROBOTS_INCLUDE_DIR
  src/include
)

set(BEHAVIOR_INFORM_ROBOTS_TEST_DIR
  src/test
)


# Files declaration
set(BEHAVIOR_INFORM_ROBOTS_SOURCE_FILES
  ${BEHAVIOR_INFORM_ROBOTS_SOURCE_DIR}/behavior_inform_robots.cpp
  ${BEHAVIOR_INFORM_ROBOTS_SOURCE_DIR}/behavior_inform_robots_main.cpp
)

set(BEHAVIOR_INFORM_ROBOTS_HEADER_FILES
  ${BEHAVIOR_INFORM_ROBOTS_INCLUDE_DIR}/behavior_inform_robots.h
)

### Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED
  COMPONENTS
  roscpp
  std_msgs
  behavior_process
  aerostack_msgs
  droneMsgsROS 
)

###################################
## catkin specific configuration ##
###################################
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS ${BEHAVIOR_INFORM_ROBOTS_INCLUDE_DIR}
  CATKIN_DEPENDS
  roscpp
  std_msgs
  behavior_process
  aerostack_msgs
  droneMsgsROS  DEPENDS yaml-cpp

)

###########
## Build ##
###########
include_directories(
  ${BEHAVIOR_INFORM_ROBOTS_INCLUDE_DIR}
  ${BEHAVIOR_INFORM_ROBOTS_SOURCE_DIR}
)
include_directories(
  ${catkin_INCLUDE_DIRS}
)

add_library(behavior_inform_robots_lib ${BEHAVIOR_INFORM_ROBOTS_SOURCE_FILES} ${BEHAVIOR_INFORM_ROBOTS_HEADER_FILES})
add_dependencies(behavior_inform_robots_lib ${catkin_EXPORTED_TARGETS})
target_link_libraries(behavior_inform_robots_lib ${catkin_LIBRARIES})
target_link_libraries(behavior_inform_robots_lib yaml-cpp)

add_executable(behavior_inform_robots ${BEHAVIOR_INFORM_ROBOTS_SOURCE_DIR}/behavior_inform_robots_main.cpp)
add_dependencies(behavior_inform_robots ${catkin_EXPORTED_TARGETS})
target_link_libraries(behavior_inform_robots behavior_inform_robots_lib)
target_link_libraries(behavior_inform_robots ${catkin_LIBRARIES})

#############
## Testing ##
#############
if (CATKIN_ENABLE_TESTING)
  catkin_add_gtest(behavior_inform_robots_test ${BEHAVIOR_INFORM_ROBOTS_TEST_DIR}/behavior_inform_robots_test.cpp)
  target_link_libraries(behavior_inform_robots_test behavior_inform_robots_lib)
  target_link_libraries(behavior_inform_robots_test ${catkin_LIBRARIES})

endif()


