/*!*******************************************************************************************
 *  \file       behavior_self_localize_and_map_with_lidar.cpp
 *  \brief      Behavior Self Localize and Map with Lidar implementation file.
 *  \details    This file implements the BehaviorSelfLocalizeAndMapWithLidar class.
 *  \authors    Raul Cruz
 *  \maintainer Raul Cruz
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/behavior_self_localize_and_map_with_lidar.h"

BehaviorSelfLocalizeAndMapWithLidar::BehaviorSelfLocalizeAndMapWithLidar() : BehaviorProcess()
{
  hector_reset_msg.data = "reset";
}

BehaviorSelfLocalizeAndMapWithLidar::~BehaviorSelfLocalizeAndMapWithLidar()
{
}

void BehaviorSelfLocalizeAndMapWithLidar::ownSetUp(){
  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory, "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack");

  private_nh.param<std::string>("hector_map_topic", hector_map_topic_str, "hector_map");
  private_nh.param<std::string>("output_map_topic", output_map_topic_str, "hector_map");

  is_stopped = false;
}


void BehaviorSelfLocalizeAndMapWithLidar::ownStart(){
  //Subscribers
  hectorMapSub  = node_handle.subscribe(hector_map_topic_str, 1, &BehaviorSelfLocalizeAndMapWithLidar::hectorMapCallback, this);
  mapPub = node_handle.advertise<nav_msgs::OccupancyGrid>(output_map_topic_str, 1, false);

  hectorReset = node_handle.advertise<std_msgs::String>("syscommand", 1, false);

  is_stopped = false;
  // start processes is done automatically With the parent class
  hectorReset.publish(hector_reset_msg);
}

void BehaviorSelfLocalizeAndMapWithLidar::ownRun(){}
void BehaviorSelfLocalizeAndMapWithLidar::ownStop(){
  is_stopped = true;
  hectorMapSub.shutdown();
  mapPub.shutdown();
  // stop process to droneRobotLocalizationROS and ekf_localization is done automatically With the parent class
  hectorReset.publish(hector_reset_msg);
}

std::tuple<bool,std::string> BehaviorSelfLocalizeAndMapWithLidar::ownCheckActivationConditions(){
  return std::make_tuple(true,"");
}

void BehaviorSelfLocalizeAndMapWithLidar::hectorMapCallback(const nav_msgs::OccupancyGrid &msg) {
  if( !is_stopped ){
    mapPub.publish(msg);
  }
}

