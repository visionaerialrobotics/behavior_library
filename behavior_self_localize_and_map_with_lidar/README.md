# behavior_self_localize_and_map_with_lidar

**Purpose**: The robot self localizes and maps the environment using lidar. The resulting map is represented with an occupancy grid.

**Type of behavior:** Recurrent behavior.

**Arguments:** None.

# Diagram

![Diagram self localize and map.png](https://bitbucket.org/repo/rokr9B/images/4075234933-Diagram%20self%20localize%20and%20map.png)

----
# Contributors

**Code maintainer:** Raúl Cruz

**Authors:** Raúl Cruz
