# go_to_point_with_occupancy_grid

**Purpose**: The robot goes to a destination point avoiding obstacles. The point can be expressed in absolute coordinates (argument coordinates). To reach the target point, the robot vertically changes the altitude to be at the same altitude as the target point and, then, follows a two-dimensional (2D) movement in the horizontal plane avoiding existing obstacles. During the movement, the robot can dynamically change the planned path if a new obstacle (e.g., another robot) is detected.

**Type of behavior:** Goal-based behavior.

**Arguments:** 

| Name    |   Format  |  Example |  
| :-----------| :---------| :--------|
| coordinates: |Coordinates x, y, z (meters)|coordinates: [1.23, 2, 0.5]|	

# Diagram

![go_to_point_with_occupancy_grid.png](https://bitbucket.org/repo/rokr9B/images/405842667-go_to_point_with_occupancy_grid.png)
----
# Contributors

**Code maintainer:** Ra�l Cruz

**Authors:** Ra�l Cruz

