/*!*******************************************************************************************
 *  \file       behavior_go_to_point_with_occupancy_grid.cpp
 *  \brief      Behavior Go To Point With Occupancy Grid implementation file.
 *  \details    This file implements the BehaviorGoToPointWithOccupancyGrid class.
 *  \authors    Raul Cruz
 *  \maintainer Raul Cruz
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/behavior_go_to_point_with_occupancy_grid.h"

BehaviorGoToPointWithOccupancyGrid::BehaviorGoToPointWithOccupancyGrid() : BehaviorProcess(){}
BehaviorGoToPointWithOccupancyGrid::~BehaviorGoToPointWithOccupancyGrid(){}

void BehaviorGoToPointWithOccupancyGrid::ownSetUp(){
  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory, "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack");

  private_nh.param<std::string>("generated_path_topic", generated_path_topic_str, "path_with_id");
  private_nh.param<std::string>("generate_path_service", generate_path_service_str, "generate_path");
  private_nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "EstimatedPose_droneGMR_wrt_GFF");
  private_nh.param<std::string>("yaw_controller_topic", yaw_controller_str , "droneControllerYawRefCommand");
  private_nh.param<std::string>("trajectory_topic", trajectory_topic, "droneTrajectoryAbsRefCommand");
  private_nh.param<std::string>("consult_belief", execute_query_srv,"consult_belief");
  private_nh.param<std::string>("controllers_topic", controllers_topic, "command/high_level");
  dumpTopicNames();
}

void BehaviorGoToPointWithOccupancyGrid::ownStart(){
  std::cout << "ownStart" << std::endl;

  is_finished = false;
  bad_args = false;
  obstacle = false;
  changing_yaw = false;
  pending_path = true;
  yaw_sent = false;

  setupTopics();
  setupStartPosition(estimated_pose);

  // Extract target position
  if( !grabInputPose(estimated_pose, target_position) ){
    setStarted(false);
    bad_args = true;
    is_finished = true;
    return;
  }

  setStarted(true);

  target_yaw = fixYaw(target_position.yaw);
  current_yaw = fixYaw(estimated_pose.yaw);
  
  changing_yaw = false;
  path_id = requestPath(target_position);
  return;

  yawOrRequest();
}

void BehaviorGoToPointWithOccupancyGrid::ownRun(){
  is_finished = goalReached(estimated_pose, target_position);
  if( timerIsFinished() || (is_finished && bad_args) ){
    std::cout<< "Behavior finished WITH error" << std::endl;
    BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
    BehaviorProcess::setFinishConditionSatisfied(true);
  }else if( obstacle || is_finished ){
    std::cout<< "Behavior finished WITHOUT error" << std::endl;
    BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
    BehaviorProcess::setFinishConditionSatisfied(true);
  }
}

void BehaviorGoToPointWithOccupancyGrid::ownStop()
{
  path_sub.shutdown();
  path_client.shutdown();
  estimated_pose_sub.shutdown();
  yaw_controller_pub.shutdown();
  trajectory_pub.shutdown();
  belief_manager_client.shutdown();
  controllers_pub.shutdown();
}

std::tuple<bool,std::string> BehaviorGoToPointWithOccupancyGrid::ownCheckActivationConditions(){
  
  droneMsgsROS::ConsultBelief query_service;
  query_service.request.query = "battery_level(self,LOW)";

  if( belief_manager_client.call(query_service) == 0 ){
    std::cout<<"Warning: Belief Query service not ready, try again..."<<std::endl;
  }

  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Battery low, unable to perform action");
  }

  query_service.request.query = "flight_state(self,LANDED)";

  if( belief_manager_client.call(query_service) == 0 ){
    std::cout<<"Warning: Belief Query service not ready, try again..."<<std::endl;
  }

  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Drone landed");
  }
  return std::make_tuple(true,"");
}

// Callbacks
void BehaviorGoToPointWithOccupancyGrid::generatedPathCallback(const aerostack_msgs::PathWithID &resp_path){
  std::cout<< "Path is back id (" << resp_path.uid << "), our id (" << path_id << ")" <<std::endl;
  if( resp_path.uid != path_id ){
    // not for us
    return;
  }
  std::cout<< "Path is back size " << resp_path.poses.size() <<"\n";
  // No need to convert, just reusability
  nav_msgs::Path nav_path;
  nav_path.poses = resp_path.poses;
  setupTrajectory(current_traj);
  if( nav_path.poses.size() > 0 ){
    convertPath(nav_path, current_traj);
  }else{
    // Obstacle, stop!
    obstacle = true;
    droneMsgsROS::dronePositionRefCommand waypoint;
    waypoint.x = estimated_pose.x;
    waypoint.y = estimated_pose.y;
    waypoint.z = estimated_pose.z;
    // waypoint.yaw = target_yaw;
    current_traj.droneTrajectory.push_back(waypoint);
  }
  sendTrajectory(current_traj);
  // BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
  // BehaviorProcess::setFinishConditionSatisfied(true);
}

void BehaviorGoToPointWithOccupancyGrid::estimatedPoseCallBack(const droneMsgsROS::dronePose& msg){
  estimated_pose = msg;
  current_yaw = fixYaw(estimated_pose.yaw);
  /*if( is_finished = goalReached(estimated_pose, target_position) ){
    return;
  }*/
  /*if( pending_path || changing_yaw ){
    if( !yaw_sent ){
      yawOrRequest();
      sendYaw(target_yaw);
    }
  }*/
  // else executing trajectory
}

// Convert a navigation path to an aerostack one
void BehaviorGoToPointWithOccupancyGrid::convertPath(
  const nav_msgs::Path &path,
  droneMsgsROS::dronePositionTrajectoryRefCommand &return_path
){
  for(int i = 0; i < path.poses.size(); i++){
    droneMsgsROS::dronePositionRefCommand next_waypoint;
    next_waypoint.x = path.poses[i].pose.position.x;
    next_waypoint.y = path.poses[i].pose.position.y;
    next_waypoint.z = target_position.z;
    // ToDo := Is yaw necessary here?
    return_path.droneTrajectory.push_back(next_waypoint);
  }
}

// Extract input arguments
bool BehaviorGoToPointWithOccupancyGrid::grabInputPose(const droneMsgsROS::dronePose estimated_pose, droneMsgsROS::dronePose &position){
  // Extract target position
  std::string arguments = getArguments();
  YAML::Node config_file = YAML::Load(arguments);
  if(!config_file["coordinates"].IsDefined() && !config_file["relative_coordinates"].IsDefined()){
    std::cout<< "Null point" <<std::endl;
    return false;
  }else if( config_file["coordinates"].IsDefined() ){
    std::vector<double> points = config_file["coordinates"].as<std::vector<double>>();
    position.x   = std::isinf(points[0]) ? estimated_pose.x   : points[0];
    position.y   = std::isinf(points[1]) ? estimated_pose.y   : points[1];
    position.z   = std::isinf(points[2]) ? estimated_pose.z   : points[2];
    position.yaw = std::isinf(points[3]) ? estimated_pose.yaw : points[3];
    std::cout<< "Got point ["<<position.x << ", "<< position.y << ", " << position.z << ", " << position.yaw <<"]"<<std::endl;
  }else{
    std::vector<double> points = config_file["relative_coordinates"].as<std::vector<double>>();
    position.x   = std::isinf(points[0]) ? estimated_pose.x    : points[0] + estimated_pose.x;
    position.y   = std::isinf(points[1]) ? estimated_pose.y    : points[1] + estimated_pose.y;
    position.z   = std::isinf(points[2]) ? estimated_pose.z    : points[2] + estimated_pose.z;
    position.yaw = std::isinf(points[3]) ? estimated_pose.yaw  : points[3] + estimated_pose.yaw;
    std::cout<< "Got Relative point ["<<position.x << ", "<< position.y << ", " << position.z << ", " << position.yaw <<"]"<<std::endl;
  }
  return true;
}

// Send point to request path
int BehaviorGoToPointWithOccupancyGrid::requestPath(droneMsgsROS::dronePose position){

  // ask for a path to be generated
  aerostack_msgs::GeneratePath path_msg;

  geometry_msgs::PoseStamped target_stamped;
  target_stamped.pose.position.x = position.x;
  target_stamped.pose.position.y = position.y;
  target_stamped.pose.position.z = position.z;

  // ToDo := Yaw necessary?

  path_msg.request.goal = target_stamped;
  path_client.call(path_msg);
  int id = path_msg.response.uid;

  pending_path = false;

  std::cout<< "Run on point, got id "<< id << " name " << path_client.getService() <<std::endl;
  return id;
}


void BehaviorGoToPointWithOccupancyGrid::setControllerMode(){
  // set trajectory controller mode to move and wait for the change
  droneMsgsROS::droneCommand msg;
  std_msgs::Header header;
  header.frame_id = "behavior_go_to_point_in_occupancy_grid";
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::MOVE;
  controllers_pub.publish(msg);

  // Wait for controller to change mode
  ros::topic::waitForMessage<droneMsgsROS::droneTrajectoryControllerControlMode>(
    "droneTrajectoryController/controlMode", node_handle
  );
}

void BehaviorGoToPointWithOccupancyGrid::setupTrajectory(
  droneMsgsROS::dronePositionTrajectoryRefCommand &return_path
){
  return_path = droneMsgsROS::dronePositionTrajectoryRefCommand();
  return_path.header.stamp = ros::Time::now();
  return_path.initial_checkpoint = 0;
  return_path.is_periodic = false;
  return_path.droneTrajectory.clear();
}

void BehaviorGoToPointWithOccupancyGrid::setupTopics(){
  // Initialize topics
  path_sub = node_handle.subscribe(generated_path_topic_str, 1, &BehaviorGoToPointWithOccupancyGrid::generatedPathCallback, this);
  path_client = node_handle.serviceClient<aerostack_msgs::GeneratePath>(generate_path_service_str);
  estimated_pose_sub = node_handle.subscribe(estimated_pose_str, 1000, &BehaviorGoToPointWithOccupancyGrid::estimatedPoseCallBack, this);
  yaw_controller_pub = node_handle.advertise<droneMsgsROS::droneYawRefCommand>(yaw_controller_str, 1);
  trajectory_pub = node_handle.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>(trajectory_topic, 1);
  belief_manager_client = node_handle.serviceClient <droneMsgsROS::ConsultBelief> (execute_query_srv);
  controllers_pub = node_handle.advertise<droneMsgsROS::droneCommand>(controllers_topic, 1000, true);
}

void BehaviorGoToPointWithOccupancyGrid::setupStartPosition(droneMsgsROS::dronePose &position){
  // Get an initial estimate, just in case of a relative point
  position.x = 0;
  position.y = 0;
  position.z = 0;
  position = *ros::topic::waitForMessage<droneMsgsROS::dronePose>(estimated_pose_str, node_handle);
}

void BehaviorGoToPointWithOccupancyGrid::sendTrajectory(droneMsgsROS::dronePositionTrajectoryRefCommand &traj){
  setControllerMode();
  std::cout<<"Trajectory sent with (" << traj.droneTrajectory.size() << ") points"<<std::endl;
  trajectory_pub.publish(traj);
}

double BehaviorGoToPointWithOccupancyGrid::fixYaw(double in_yaw){
  double ret_yaw = in_yaw;
  if( in_yaw < 0){
    ret_yaw += 2 * M_PI;  //No angles lower < 0 
  }
  return ret_yaw;
}

void BehaviorGoToPointWithOccupancyGrid::sendYaw(double yaw){
  setControllerMode();
  changing_yaw = true;
  // Ask for a move in yaw
  droneMsgsROS::droneYawRefCommand final_yaw;
  final_yaw.header.stamp = ros::Time::now();
  final_yaw.yaw = yaw;
  yaw_controller_pub.publish(final_yaw);
  yaw_sent = true;
}

bool BehaviorGoToPointWithOccupancyGrid::goalReached(droneMsgsROS::dronePose current, droneMsgsROS::dronePose target){
  /*std::cout<<"Goal "<< fabs(current.x - target.x);
  std::cout<<", "<< fabs(current.y - target.y);
  std::cout<<", "<< fabs(current.z - target.z)<<std::endl;*/
  return (
    fabs(current.x - target.x) < DIST_TOL &&
    fabs(current.y - target.y) < DIST_TOL &&
    fabs(current.z - target.z) < DIST_TOL
    // && fabs(current.yaw - target.yaw) < YAW_TOL
  );
}

void BehaviorGoToPointWithOccupancyGrid::yawOrRequest(){
  if( fabs(target_yaw - current_yaw) > YAW_TOL ){
    std::cout<< "Correcting yaw first..." << std::endl;
    std::cout<< "Requested (" << target_yaw << ") current(" << current_yaw << ")" << std::endl;
    changing_yaw = true;
    // Sleep a moment and ask again
    // sendYaw(target_yaw);
    // if( fabs(target_yaw - current_yaw) > YAW_TOL){
    //   ros::spinOnce();
    // }
  }else if( pending_path ){
    std::cout<< "Requesting path" << std::endl;
    changing_yaw = false;
    path_id = requestPath(target_position);
  }
}

void BehaviorGoToPointWithOccupancyGrid::dumpTopicNames(){
  std::cout<<"generated_path_topic_str "<<generated_path_topic_str<<std::endl;
  std::cout<<"generate_path_service_str "<<generate_path_service_str<<std::endl;
  std::cout<<"estimated_pose_str "<<estimated_pose_str<<std::endl;
  std::cout<<"yaw_controller_str "<<yaw_controller_str<<std::endl;
  std::cout<<"trajectory_topic "<<trajectory_topic<<std::endl;
  std::cout<<"execute_query_srv "<<execute_query_srv<<std::endl;
  std::cout<<"controllers_topic "<<controllers_topic<<std::endl;
}
