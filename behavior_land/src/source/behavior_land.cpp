/*!*******************************************************************************************
 *  \file       behavior_land.cpp
 *  \brief      Behavior Land implementation file.
 *  \details    This file implements the behaviorLand class.
 *  \authors    Rafael Artiñano Muñoz
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "behavior_land.h"



BehaviorLand::BehaviorLand(): BehaviorProcess()
{


}

BehaviorLand::~BehaviorLand()
{

}

void BehaviorLand::ownSetUp()
{
  std::cout << "ownSetup" << std::endl;

  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory,
                                 "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");
  private_nh.param<std::string>("behavior_name", behavior_name_str, "LAND");

  private_nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "estimated_pose");
  private_nh.param<std::string>("controllers_topic", controllers_str, "command/high_level");
  private_nh.param<std::string>("estimated_speed_topic",estimated_speed_str,"estimated_speed");
  private_nh.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
  query_client = node_handle.serviceClient <droneMsgsROS::ConsultBelief> (execute_query_srv);
}
void BehaviorLand::ownStart()
{
  is_finished=false;
  std::cout << "ownStart" << std::endl;
  /*Initialize topics*/
  estimated_pose_sub = node_handle.subscribe(estimated_pose_str, 1000, &BehaviorLand::estimatedPoseCallBack, this);
  estimated_speed_sub = node_handle.subscribe(estimated_speed_str, 1000, &BehaviorLand::estimatedSpeedCallback, this);
  controllers_pub = node_handle.advertise<droneMsgsROS::droneCommand>(controllers_str, 1, true);


  /*behavior implementation*/
  std_msgs::Header header;
  header.frame_id = "behavior_land";

  droneMsgsROS::droneCommand msg;
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::LAND;
  controllers_pub.publish(msg);

  estimated_pose_msg.z = 1;
  estimated_speed_msg.dz = 1;
  // staticity_timer.time(5);
  static_pose.z=1;
  first_position=false;
}
void BehaviorLand::ownRun()
{
  double precision_land_pose = 0.01;
  double precision_land_speed = 0.1;
  if(!is_finished){
    //Check achievement
    if(estimated_pose_msg.z < precision_land_pose /*&& std::abs(estimated_speed_msg.dz)< precision_land_speed*/) {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }

    //Check timeout
    if(timerIsFinished()){
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
    /* if(staticity_timer.isFinished() && std::abs(static_pose.z-estimated_pose_msg.z)<precision_land_pose)
    {
      BehaviorProcess::setFinishEvent(droneMsgsROS::behaviorEvent::BLOCKED);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
   if( std::abs(estimated_pose_msg.z) < precision_land_pose && std::abs(estimated_speed_msg.dz)> precision_land_speed)
   {
      BehaviorProcess::setFinishEvent(droneMsgsROS::behaviorEvent::WRONG_PROGRESS);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
      }*/

  }

}
std::tuple<bool,std::string> BehaviorLand::ownCheckActivationConditions()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador<<"flight_state(self,LANDED)";
  std::string query2(capturador.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Drone landed");
    //return false;
  }

  return std::make_tuple(true,"");
  //return true;
}
void BehaviorLand::ownStop()
{
  std::cout << "ownStop" << std::endl;
  std_msgs::Header header;
  header.frame_id = "behavior_land";

  droneMsgsROS::droneCommand msg;
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::HOVER;
  controllers_pub.publish(msg);
  estimated_pose_sub.shutdown();
  controllers_pub.shutdown();
  estimated_speed_sub.shutdown();
  //staticity_timer.stop();
}

void BehaviorLand::estimatedPoseCallBack(const droneMsgsROS::dronePose& msg)
{
  if(!first_position)
        static_pose=msg;
  estimated_pose_msg=msg;

}
void BehaviorLand::estimatedSpeedCallback(const droneMsgsROS::droneSpeeds& msg)
{
  estimated_speed_msg=msg;

}
