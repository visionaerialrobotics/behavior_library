# Brief
Let the drone reach the ground.

# Subscribed Topics

- **estimated_pose** ([droneMsgsROS/dronePose](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/dronePose.msg))  
Contains the estimated pose of the drone.

# Published Topics

- **command/high_level** ([droneMsgsROS/droneCommand](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/droneCommand.msg))  
Publishes the new state of the drone.

# Services

- **consult_belief** ([droneMsgsROS/ConsultBelief](https://bitbucket.org/joselusl/dronemsgsros/src/master/srv/ConsultBelief.srv))  
A service used for retrieving information about the drone. It gets information about the battery level and the flight status.

# Diagram

![land diagram](img/diagram.png)

# Contributors

Maintainer: Alberto Camporredondo (alberto.camporredondop@alumnos.upm.es)

Author: Rafael Artiñano
