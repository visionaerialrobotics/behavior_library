cmake_minimum_required(VERSION 2.8.3)
project(behavior_generate_path_in_geometric_map)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED)

add_definitions(-std=c++11)

# Directories definition
set(BEHAVIOR_GENERATE_PATH_SOURCE_DIR
  src/source
)

set(BEHAVIOR_GENERATE_PATH_INCLUDE_DIR
  src/include
)

# Files declaration
set(BEHAVIOR_GENERATE_PATH_SOURCE_FILES
  ${BEHAVIOR_GENERATE_PATH_SOURCE_DIR}/behavior_generate_path_in_geometric_map.cpp
  ${BEHAVIOR_GENERATE_PATH_SOURCE_DIR}/behavior_generate_path_in_geometric_map_main.cpp
)

set(BEHAVIOR_GENERATE_PATH_HEADER_FILES
  ${BEHAVIOR_GENERATE_PATH_INCLUDE_DIR}/behavior_generate_path_in_geometric_map.h
)

### Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED
  COMPONENTS
  roscpp
  std_msgs
  behavior_process
  droneMsgsROS
  aerostack_msgs
)

###################################
## catkin specific configuration ##
###################################
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS ${BEHAVIOR_GENERATE_PATH_INCLUDE_DIR}
  CATKIN_DEPENDS
  roscpp
  std_msgs
  drone_process
  behavior_process
  droneMsgsROS
  aerostack_msgs
  DEPENDS yaml-cpp eigen
)

###########
## Build ##
###########
include_directories(
  ${BEHAVIOR_GENERATE_PATH_INCLUDE_DIR}
  ${BEHAVIOR_GENERATE_PATH_SOURCE_DIR}
)
include_directories(
  ${catkin_INCLUDE_DIRS}
)

add_library(behavior_generate_path_in_geometric_map_lib ${BEHAVIOR_GENERATE_PATH_SOURCE_FILES} ${BEHAVIOR_GENERATE_PATH_HEADER_FILES})
add_dependencies(behavior_generate_path_in_geometric_map_lib ${catkin_EXPORTED_TARGETS})
target_link_libraries(behavior_generate_path_in_geometric_map_lib ${catkin_LIBRARIES})
target_link_libraries(behavior_generate_path_in_geometric_map_lib yaml-cpp)

add_executable(behavior_generate_path_in_geometric_map ${BEHAVIOR_GENERATE_PATH_SOURCE_DIR}/behavior_generate_path_in_geometric_map_main.cpp)
add_dependencies(behavior_generate_path_in_geometric_map ${catkin_EXPORTED_TARGETS})
target_link_libraries(behavior_generate_path_in_geometric_map behavior_generate_path_in_geometric_map_lib)
target_link_libraries(behavior_generate_path_in_geometric_map ${catkin_LIBRARIES})

#############
## Testing ##
#############
#if (CATKIN_ENABLE_TESTING)
#  catkin_add_gtest(BehaviorManagerTest ${BEHAVIOR_DESCRIPTOR_TEST_DIR}/behavior_manager_test.cpp)
#  target_link_libraries(BehaviorManagerTest behavior_take_off_lib)
#  target_link_libraries(BehaviorManagerTest ${catkin_LIBRARIES})

  #find_package(rostest REQUIRED)
  #add_rostest(launch/lau.test)
#endif()
