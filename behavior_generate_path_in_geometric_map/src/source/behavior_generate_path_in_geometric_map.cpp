#include<behavior_generate_path_in_geometric_map.h>
BehaviorGeneratePathInGeometicMap::BehaviorGeneratePathInGeometicMap():BehaviorProcess()
{

}

BehaviorGeneratePathInGeometicMap::~BehaviorGeneratePathInGeometicMap()
{

}

void BehaviorGeneratePathInGeometicMap::ownSetUp()
{
  std::cout << "ownSetup" << std::endl;

  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory,
                                 "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");

  private_nh.param<std::string>("trajectory_generated_topic",trajectory_generated_str,"droneTrajectoryAbsRefGenerated");
  private_nh.param<std::string>("mission_point",mission_point, "droneMissionPoint");
  private_nh.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
  private_nh.param<std::string>("add_belief",add_belief,"add_belief");



}

void BehaviorGeneratePathInGeometicMap::ownStart()
{
  first=0;
  path_generated=false;
  is_finished=false;
  reference_trajectory_pub = node_handle.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>(trajectory_ref_command_str,100);
  trajectory_pub = node_handle.subscribe(trajectory_generated_str,1,&BehaviorGeneratePathInGeometicMap::trajectory_callback,this);
  query_client = node_handle.serviceClient <droneMsgsROS::ConsultBelief> (execute_query_srv);
  add_belief_service = node_handle.serviceClient<droneMsgsROS::AddBelief>(add_belief);
  mission_point_pub = node_handle.advertise<droneMsgsROS::dronePositionRefCommand>(mission_point,1);
  std::cout << "ownStart" << std::endl;
  std::string arguments = getArguments();
  YAML::Node config_file = YAML::Load(arguments);
  if(config_file["destination"].IsDefined()){
    std::vector<double> points=config_file["coordinates"].as<std::vector<double>>();
    target_position.x=points[0];
    target_position.y=points[1];
    target_position.z=points[2];
  }
    else{
      setStarted(false);
      return;
    }
  droneMsgsROS::dronePositionRefCommand mission_point;
  mission_point.x=estimated_pose_msg.x;
  mission_point.y=estimated_pose_msg.y;
  mission_point.z=estimated_pose_msg.z;
  mission_point_pub.publish(mission_point);
  mission_point.x=target_position.x;
  mission_point.y=target_position.y;
  mission_point.z=target_position.z;
  mission_point_pub.publish(mission_point);

  /*Initialize topics*/
}

void BehaviorGeneratePathInGeometicMap::ownRun()
{
  if(is_finished==false)
  {
    if(path_generated)
    {
      std::cout<< "entra en el goal archived"<<std::endl;
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
    if(timerIsFinished())
    {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
}
}
std::tuple<bool,std::string> BehaviorGeneratePathInGeometicMap::ownCheckActivationConditions()
{

  return std::make_tuple(true,"");
  //return true;
}
void BehaviorGeneratePathInGeometicMap::ownStop()
{

}
void BehaviorGeneratePathInGeometicMap::estimatedPoseCallBack(const droneMsgsROS::dronePose& msg)
{
  estimated_pose_msg=msg;
}
void BehaviorGeneratePathInGeometicMap::trajectory_callback(const droneMsgsROS::dronePositionTrajectoryRefCommand & msg)
{

  std::string str =  "trajectory(self,(";
  for(int i=0;i<msg.droneTrajectory.size();i++){
    if(i==0){
      std::ostringstream osx ;
      osx << msg.droneTrajectory[i].x;
      std::ostringstream osy ;
      osy << msg.droneTrajectory[i].y;
      std::ostringstream osz ;
      osz << msg.droneTrajectory[i].z;
    std::string ss="( " +osx.str()+", "+ osy.str()+", "+osz.str()+" )";
    str=str + ss;}
    else{
      std::ostringstream osx ;
      osx << msg.droneTrajectory[i].x;
      std::ostringstream osy ;
      osy << msg.droneTrajectory[i].y;
      std::ostringstream osz ;
      osz << msg.droneTrajectory[i].z;
      std::string ss=", ( " +osx.str()+", "+ osy.str()+", "+osz.str()+" )";
      str=str +ss;
    }
  }
  str=str+"))";
  std::cout<< "el ultimo punto corresponde a:"<< str << std::endl;
  if(!path_generated && msg.droneTrajectory[msg.droneTrajectory.size()-1].x==(long)target_position.x && msg.droneTrajectory[msg.droneTrajectory.size()-1].y==(long)target_position.y && msg.droneTrajectory[msg.droneTrajectory.size()-1].z==(long)target_position.z){
    droneMsgsROS::AddBelief add_belief_msg;
    str =  "path_to_follow(self,(";
    for(int i=0;i<msg.droneTrajectory.size();i++){
      if(i==0){
        std::ostringstream osx ;
        osx << msg.droneTrajectory[i].x;
        std::ostringstream osy ;
        osy << msg.droneTrajectory[i].y;
        std::ostringstream osz ;
        osz << msg.droneTrajectory[i].z;
      std::string ss="( " +osx.str()+", "+ osy.str()+", "+osz.str()+" )";
      str=str + ss;}
      else{
        std::ostringstream osx ;
        osx << msg.droneTrajectory[i].x;
        std::ostringstream osy ;
        osy << msg.droneTrajectory[i].y;
        std::ostringstream osz ;
        osz << msg.droneTrajectory[i].z;
        std::string ss=", ( " +osx.str()+", "+ osy.str()+", "+osz.str()+" )";
        str=str +ss;
      }
    }
    str=str+"))";
    add_belief_msg.request.multivalued = false;
    add_belief_msg.request.belief_expression = str;
    std::cout<<str<<std::endl;
    add_belief_service.call(add_belief_msg);
    path_generated=true;
  }
}
