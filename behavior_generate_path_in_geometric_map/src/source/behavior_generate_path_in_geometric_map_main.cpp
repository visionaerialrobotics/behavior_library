#include "../include/behavior_generate_path_in_geometric_map.h"
#include <boost/thread/thread.hpp>

int main(int argc, char** argv){
  ros::init(argc, argv, ros::this_node::getName());

  std::cout << ros::this_node::getName() << std::endl;

  BehaviorGeneratePathInGeometicMap behavior;
  behavior.setUp();

  ros::Rate rate(10);
  while(ros::ok()){
    ros::spinOnce();
    behavior.run();
    rate.sleep();
  }
  return 0;
}
