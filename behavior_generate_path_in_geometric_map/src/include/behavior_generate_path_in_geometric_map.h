#ifndef BEHAVIOR_GENERATE_PATH_H
#define BEHAVIOR_GENERATE_PATH_H

// System
#include <string>
#include <iostream>
#include <tuple>
#include <map>
#include <yaml-cpp/yaml.h>
#include <signal.h>


// ROS
#include <ros/ros.h>

// Aerostack libraries
#include <behavior_process.h>

//Msgs
#include <aerostack_msgs/BehaviorEvent.h>
#include <std_msgs/Bool.h>
#include <droneMsgsROS/ConsultBelief.h>
#include <droneMsgsROS/AddBelief.h>
#include <droneMsgsROS/RemoveBelief.h>
#include <boost/range/combine.hpp>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <boost/algorithm/string/replace.hpp>
#include <droneMsgsROS/AddBelief.h>
#include <droneMsgsROS/ConsultIncompatibleBehaviors.h>
#include <aerostack_msgs/BehaviorCommand.h>
#include <aerostack_msgs/RequestBehavior.h>
#include <droneMsgsROS/dronePose.h>
#include <tuple>

class OperatorAssistanceWidget;

class BehaviorGeneratePathInGeometicMap: public BehaviorProcess
{
public:
  struct Behavior
  {
    std::string name;
    std::string arguments;
  };


public:
  BehaviorGeneratePathInGeometicMap();
  ~BehaviorGeneratePathInGeometicMap();

private:
  ros::NodeHandle node_handle;

  bool finished;
  bool first=0;

  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;


  std::string add_belief;
  std::string execute_query_srv;
  std::string trajectory_generated_str;
  std::string mission_point;
  std::string trajectory_ref_command_str;
  bool path_generated=false;
  bool is_finished;
  ros::Publisher reference_trajectory_pub;
  ros::Publisher mission_point_pub;
  ros::Subscriber trajectory_pub;
  ros::ServiceClient query_client;
  ros::ServiceClient add_belief_service;
  droneMsgsROS::dronePose target_position;
  droneMsgsROS::dronePose estimated_pose_msg;
  droneMsgsROS::dronePose static_pose;

// BehaviorProcess
private:
  void ownSetUp();
  void ownStart();
  void ownRun();
  void ownStop();
  std::tuple<bool, std::string> ownCheckActivationConditions();
  void trajectory_callback(const droneMsgsROS::dronePositionTrajectoryRefCommand &);
  void estimatedPoseCallBack(const droneMsgsROS::dronePose&);



};

#endif
