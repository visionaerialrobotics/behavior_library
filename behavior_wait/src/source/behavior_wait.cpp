/*!*******************************************************************************************
 *  \file       behavior_wait.cpp
 *  \brief      Behavior Wait implementation file.
 *  \details    This file implements the behaviorWait class.
 *  \authors    Rafael Artiñano Muñoz
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/behavior_wait.h"
BehaviorWait::BehaviorWait():BehaviorProcess()
{

}

BehaviorWait::~BehaviorWait()
{

}

void BehaviorWait::ownSetUp()
{
  timer_msg=false;
  std::cout << "ownSetup" << std::endl;

  node_handle.param<std::string>("drone_id", drone_id, "1");
  node_handle.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  node_handle.param<std::string>("my_stack_directory", my_stack_directory,
                  "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");

  node_handle.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
}

void BehaviorWait::ownStart()
{
  timer_msg=false;
  std::cout << "ownStart" << std::endl;
  is_finished = false;
  marker_end=false;
  timeout_end=false;
  marker_found=false;
  /*Initialize topics*/
  query_client = node_handle.serviceClient <droneMsgsROS::ConsultBelief> (execute_query_srv);



  /*behavior implementation*/
  std::string arguments = getArguments();
  YAML::Node config_file = YAML::Load(arguments);
  if(config_file["timeout"].IsDefined())
  {
    timeout=config_file["timeout"].as<float>();
    timeout_end=true;
    timer = node_handle.createTimer(ros::Duration(timeout), &BehaviorWait::timerCallback, this);
  }
  if(config_file["until_observed_visual_marker"].IsDefined())
  {
    marker=config_file["until_observed_visual_marker"].as<int>();
    marker_end=true;
  }


}

void BehaviorWait::ownRun()
{
  ros::Duration(0.2).sleep();
  isVisibleMarker();
  float angle_variation_maximum=0.5;
  float speed_maximum_at_end=0.1;
  if(is_finished==false)
  {
    if(!timeout_end && timerIsFinished())
    {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
    if(timeout_end && timer_msg)
    {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
    if(marker_end && marker_found)
    {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
  }



}
std::tuple<bool, std::string> BehaviorWait::ownCheckActivationConditions()
{

  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador << "battery_level(self,LOW)";
  std::string query(capturador.str());
  query_service.request.query = query;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Battery low, unable to perform action");
    //return false;
  }
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,LANDED)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Drone landed");
    //return false;
  }
  return std::make_tuple(true,"");
  //return true;
}
void BehaviorWait::ownStop()
{
  // droneMsgsROS::dronePitchRollCmd pitch_roll_msg;
  // droneMsgsROS::droneDAltitudeCmd d_altitude_msg;
  // droneMsgsROS::droneDYawCmd d_yaw_msg;
  // droneMsgsROS::droneSpeeds reset;
  // reset.dx=0.0;
  // reset.dy=0.0;
  // reset.dz=0.0;
  // reset.dyaw=0.0;
  // reset.dpitch=0.0;
  // reset.droll=0.0;
  // pitch_roll_msg.pitchCmd = 0.0;
  // pitch_roll_msg.rollCmd = 0.0;
  // d_altitude_msg.dAltitudeCmd = 0.0;
  // d_yaw_msg.dYawCmd = 0.0;
  //
  // d_altitude_pub.publish(d_altitude_msg);
  // d_yaw_pub.publish(d_yaw_msg);
  // d_pitch_roll_pub.publish(pitch_roll_msg);
  // drone_speeds_reseter_pub.publish(reset)timer_msg
  //
  // droneMsgsROS::droneCommand msg;
  // msg.header = header;
  // msg.command = droneMsgsROS::droneCommand::HOVER;
  // controllers_pub.publish(msg);



  //staticity_timer.stop();
}
void BehaviorWait::timerCallback(const ros::TimerEvent& event)
{
timer_msg=true;
}
void BehaviorWait::isVisibleMarker()
{
  if (marker_end){
    droneMsgsROS::ConsultBelief query_service;
    std::ostringstream capturador;
    capturador << "visible(aruco_" << marker << ")";
    std::string query(capturador.str());
    query_service.request.query = query;
    query_client.call(query_service);
    marker_found=query_service.response.success;
  }

}
