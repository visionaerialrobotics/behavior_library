/*!*******************************************************************************************
 *  \file       behavior_generate_path_with_occupancy_grid.cpp
 *  \brief      Behavior Generate Path With Occupancy Grid implementation file.
 *  \details    This file implements the BehaviorGeneratePathWithOccupancyGrid class.
 *  \authors    Raul Cruz
 *  \maintainer Raul Cruz
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/behavior_generate_path_with_occupancy_grid.h"

BehaviorGeneratePathWithOccupancyGrid::BehaviorGeneratePathWithOccupancyGrid() : BehaviorProcess(){}
BehaviorGeneratePathWithOccupancyGrid::~BehaviorGeneratePathWithOccupancyGrid(){}

void BehaviorGeneratePathWithOccupancyGrid::ownSetUp(){
  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory, "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack");

  private_nh.param<std::string>("generated_path_topic_str", generated_path_topic_str, "path_with_id");
  private_nh.param<std::string>("generate_path_service_str", generate_path_service_str, "generate_path");
  private_nh.param<std::string>("add_belief_service_str", add_belief_service_str, "add_belief");

  std::cout<< "ownSetup"<<std::endl;
}

void BehaviorGeneratePathWithOccupancyGrid::ownStart(){
  is_finished = false;
  bad_args = false;
  // Extract target position
  if( !grabInputPose(target_position) ){
    setStarted(false);
    bad_args = true;
    is_finished = true;
    return;
  }

  setStarted(true);

  path_sub = node_handle.subscribe(generated_path_topic_str, 1, &BehaviorGeneratePathWithOccupancyGrid::generatedPathCallback, this);
  path_client = node_handle.serviceClient<aerostack_msgs::GeneratePath>(generate_path_service_str);
  belief_manager_client = node_handle.serviceClient<droneMsgsROS::AddBelief>(add_belief_service_str);

  path_id = requestPath(target_position);
}

void BehaviorGeneratePathWithOccupancyGrid::ownRun(){
  if( timerIsFinished() || (is_finished && bad_args) ){
    std::cout<< "Behavior finished WITH error" << std::endl;
    BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
    BehaviorProcess::setFinishConditionSatisfied(true);
  }else if( is_finished ){
    std::cout<< "Behavior finished WITHOUT error" << std::endl;
    BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
    BehaviorProcess::setFinishConditionSatisfied(true);
  }
}

void BehaviorGeneratePathWithOccupancyGrid::ownStop(){
  path_sub.shutdown();
  path_client.shutdown();
  belief_manager_client.shutdown();
  node_handle.shutdown();
}

std::tuple<bool,std::string> BehaviorGeneratePathWithOccupancyGrid::ownCheckActivationConditions(){
  return std::make_tuple(true,"");
}

// Callbacks
void BehaviorGeneratePathWithOccupancyGrid::generatedPathCallback(const aerostack_msgs::PathWithID &resp_path){
  // not for us
  std::cout<< "Path is back id (" << resp_path.uid << "), our id (" << path_id << ")" <<std::endl;
  if( resp_path.uid != path_id ){
    return;
  }
  std::cout<< "Path is back size " << resp_path.poses.size() <<"\n";
  // No need to convert, just reusability
  droneMsgsROS::dronePositionTrajectoryRefCommand path;
  nav_msgs::Path nav_path;
  nav_path.poses = resp_path.poses;
  convertPath(nav_path, path);
  std::string serialized = serializePath(requestBeliefId(), path);
  addBelief(serialized);
  is_finished = true;
  BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
  BehaviorProcess::setFinishConditionSatisfied(true);
}

///////////
// Utils //
///////////

// Get new id for a path
int BehaviorGeneratePathWithOccupancyGrid::requestBeliefId(){
  int ret = 100;
  droneMsgsROS::GenerateID::Request req;
  droneMsgsROS::GenerateID::Response res;
  ros::ServiceClient id_gen_client = node_handle.serviceClient<droneMsgsROS::GenerateID>("belief_manager_process/generate_id");
  id_gen_client.call(req, res);

  if( res.ack ){
    ret = res.id;
  }

  id_gen_client.shutdown();
  return ret;
}

// Convert a navigation path to an aerostack one
void BehaviorGeneratePathWithOccupancyGrid::convertPath(
  const nav_msgs::Path &path,
  droneMsgsROS::dronePositionTrajectoryRefCommand &return_path
){
  return_path.header.stamp = ros::Time::now();
  return_path.initial_checkpoint = 0;
  return_path.is_periodic = false;

  for(int i = 0; i < path.poses.size(); i++){
    droneMsgsROS::dronePositionRefCommand next_waypoint;
    next_waypoint.x = path.poses[i].pose.position.x;
    next_waypoint.y = path.poses[i].pose.position.y;
    next_waypoint.z = target_position.z;
    return_path.droneTrajectory.push_back(next_waypoint);
  }
}

// Convert aerostack path to string (for belief)
std::string BehaviorGeneratePathWithOccupancyGrid::serializePath(int id, droneMsgsROS::dronePositionTrajectoryRefCommand &path){
  std::ostringstream ret_stream;
  ret_stream << "path(" << id << ",(";
  for(int i = 0; i < path.droneTrajectory.size(); i++){
    ret_stream << "(";
    ret_stream << path.droneTrajectory[i].x << ",";
    ret_stream << path.droneTrajectory[i].y << ",";
    ret_stream << path.droneTrajectory[i].z;
    ret_stream << ")" << (i < path.droneTrajectory.size() -1 ? ", " : "");
  }
  ret_stream << "))";
  return ret_stream.str();
}

// Add a string belief
void BehaviorGeneratePathWithOccupancyGrid::addBelief(std::string belief){
  droneMsgsROS::AddBelief belief_msg;
  belief_msg.request.multivalued = true;
  belief_msg.request.belief_expression = belief;
  belief_manager_client.call(belief_msg);
}

// Extract input arguments
bool BehaviorGeneratePathWithOccupancyGrid::grabInputPose(droneMsgsROS::dronePose &position){
  // Extract target position
  std::string arguments = getArguments();
  YAML::Node config_file = YAML::Load(arguments);
  if(!config_file["coordinates"].IsDefined()){
    std::cout<< "Null point" <<std::endl;
    return false;
  }
  std::vector<double> points = config_file["coordinates"].as<std::vector<double>>();
  position.x = points[0];
  position.y = points[1];
  position.z = points[2];
  std::cout<< "Got point ["<<position.x << ", "<< position.y << ", " << position.z <<"]"<<std::endl;
  return true;
}

// Send point to request path
int BehaviorGeneratePathWithOccupancyGrid::requestPath(droneMsgsROS::dronePose position){

  // ask for a path to be generated
  aerostack_msgs::GeneratePath path_msg;

  geometry_msgs::PoseStamped target_stamped;
  target_stamped.pose.position.x = position.x;
  target_stamped.pose.position.y = position.y;
  target_stamped.pose.position.z = position.z;

  path_msg.request.goal = target_stamped;
  path_client.call(path_msg);
  int id = path_msg.response.uid;

  std::cout<< "Run on point, got id "<< id << " name " << path_client.getService() <<std::endl;
  return id;
}
