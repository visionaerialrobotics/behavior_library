# behavior_generate_path_with_occupancy_grid

**Purpose**: This behavior generates a path free of obstacles between the current position of the robot and the given destination.  This behavior uses a representation of the environment as an occupancy grid. The result of this task is stored in the belief memory in the predicate: path_to_follow(self, X), were X is a list of 3D points. For example: path_to_follow(self, ((1.1,1.1,3.3), (2.2,2.2,3.3), (3.3,3.3,3.3))). Note that the height value of each point (z value) is a constant value because the path is planned on a horizontal surface.

**Type of behavior:** Goal-based behavior.

**Arguments:** 

| Name    |   Format  |  Example |  
| :-----------| :---------| :--------|
| destination |Tuple of 3 numbers x, y, z (meters)|destination: [1.23, 2, 0.5]|	

# Diagram

![Diagram generate path.png](https://bitbucket.org/repo/rokr9B/images/575980004-Diagram%20generate%20path.png)
----
# Contributors

**Code maintainer:** Raúl Cruz

**Authors:** Raúl Cruz
