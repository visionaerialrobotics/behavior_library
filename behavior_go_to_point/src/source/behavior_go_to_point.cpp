/*!*******************************************************************************************
 *  \file       behavior_go_to_point.cpp
 *  \brief      Behavior Go To Point implementation file.
 *  \details    This file implements the BehaviorGoToPoint class.
 *  \authors    Rafael Artiñano Muñoz
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
#include "../include/behavior_go_to_point.h"


BehaviorGoToPoint::BehaviorGoToPoint():BehaviorProcess()
{

}

BehaviorGoToPoint::~BehaviorGoToPoint()
{

}

void BehaviorGoToPoint::ownSetUp()
{
  std::cout << "ownSetup" << std::endl;

  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory,
                                 "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");

  private_nh.param<std::string>("estimated_pose_topic", estimated_pose_str, "estimated_pose");
  private_nh.param<std::string>("controllers_topic", controllers_str, "command/high_level");
  private_nh.param<std::string>("rotation_angles_topic", rotation_angles_str, "rotation_angles");
  private_nh.param<std::string>("estimated_speed_topic",estimated_speed_str,"estimated_speed");
  private_nh.param<std::string>("yaw_controller_str",yaw_controller_str , "droneControllerYawRefCommand");
  private_nh.param<std::string>("service_topic_str",service_topic_str , "droneTrajectoryController/setControlMode");
  private_nh.param<std::string>("mission_point",mission_point, "droneMissionPoint");
  private_nh.param<std::string>("speed_reset", speed_reset_str,"droneSpeedsRefs");
  private_nh.param<std::string>("droneTrajectoryAbsRefCommand_topic", trajectory_ref_command_str, "droneTrajectoryAbsRefCommand");
  private_nh.param<std::string>("consult_belief",execute_query_srv,"consult_belief");
}

void BehaviorGoToPoint::ownStart()
{
  std::cout << "ownStart" << std::endl;
  is_finished = false;

  /*Initialize topics*/
  estimated_pose_sub = node_handle.subscribe(estimated_pose_str, 1000, &BehaviorGoToPoint::estimatedPoseCallBack, this);
  rotation_angles_sub = node_handle.subscribe(rotation_angles_str, 1000, &BehaviorGoToPoint::rotationAnglesCallback, this);
  estimated_speed_sub = node_handle.subscribe(estimated_speed_str, 1000, &BehaviorGoToPoint::estimatedSpeedCallback, this);
  controllers_pub = node_handle.advertise<droneMsgsROS::droneCommand>(controllers_str, 1000, true);
  yaw_controller_pub=node_handle.advertise<droneMsgsROS::droneYawRefCommand>(yaw_controller_str,1000);
  mode_service = node_handle.serviceClient<droneMsgsROS::setControlMode>(service_topic_str);
  mission_point_pub = node_handle.advertise<droneMsgsROS::dronePositionRefCommand>(mission_point,1000);
  drone_speeds_reseter_pub = node_handle.advertise<droneMsgsROS::droneSpeeds>(speed_reset_str, 1);
  reference_trajectory_pub = node_handle.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>(trajectory_ref_command_str,100);
  query_client = node_handle.serviceClient <droneMsgsROS::ConsultBelief> (execute_query_srv);

  estimated_pose_msg.x=0;
  estimated_pose_msg.y=0;
  estimated_pose_msg.z=0;
  estimated_pose_msg = *ros::topic::waitForMessage<droneMsgsROS::dronePose>(estimated_pose_str, node_handle);

  /*behavior implementation*/

  // Extract target position
  std::string arguments = getArguments();
  YAML::Node config_file = YAML::Load(arguments);
  if(config_file["coordinates"].IsDefined()){
    std::vector<double> points=config_file["coordinates"].as<std::vector<double>>();
    target_position.x=points[0];
    target_position.y=points[1];
    target_position.z=points[2];
  }
  else{
    if(config_file["relative_coordinates"].IsDefined())
    {
      std::vector<double> points=config_file["relative_coordinates"].as<std::vector<double>>();
      target_position.x=estimated_pose_msg.x+points[0]+0.0001;
      target_position.y=estimated_pose_msg.y+points[1];
      target_position.z=estimated_pose_msg.z+points[2];
    }
    else{
      setStarted(false);
      return;
    }
  }
  std::cout << "The trajectory sent is " << target_position << std::endl;
  // Publish controller reference
  droneMsgsROS::dronePositionTrajectoryRefCommand reference_trajectory_msg;
  droneMsgsROS::dronePositionRefCommand reference_position_msg;
  reference_trajectory_msg.header.frame_id="GO_TO_POINT";
  reference_trajectory_msg.header.stamp= ros::Time::now();
  reference_position_msg.x = estimated_pose_msg.x;
  reference_position_msg.y = estimated_pose_msg.y;
  reference_position_msg.z = estimated_pose_msg.z;
  reference_trajectory_msg.droneTrajectory.push_back(reference_position_msg);
  reference_trajectory_pub.publish(reference_trajectory_msg);

  droneMsgsROS::droneYawRefCommand reference_yaw_msg;
  reference_yaw_msg.yaw = estimated_pose_msg.yaw;
  reference_yaw_msg.header.stamp = ros::Time::now();
  yaw_controller_pub.publish(reference_yaw_msg);

  // Wait for controller to change mode
  ros::topic::waitForMessage<droneMsgsROS::droneTrajectoryControllerControlMode>(
    "droneTrajectoryController/controlMode", node_handle
  );

  // Send target point
  droneMsgsROS::dronePositionRefCommand mission_point;
  mission_point.x=estimated_pose_msg.x;
  mission_point.y=estimated_pose_msg.y;
  mission_point.z=estimated_pose_msg.z;
  mission_point_pub.publish(mission_point);

  std_msgs::Header header;
  header.frame_id = "behavior_go_to_point";

  droneMsgsROS::droneCommand msg;
  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::MOVE;
  controllers_pub.publish(msg);
  static_pose.x=estimated_pose_msg.x;
  static_pose.y=estimated_pose_msg.y;
  static_pose.z=estimated_pose_msg.z;
  std::cout << "The trajectory sent is " << target_position << std::endl;
  // Publish controller reference
  reference_trajectory_msg.header.frame_id="GO_TO_POINT";
  reference_trajectory_msg.header.stamp= ros::Time::now();
  reference_position_msg.x = estimated_pose_msg.x;
  reference_position_msg.y = estimated_pose_msg.y;
  reference_position_msg.z = estimated_pose_msg.z;
  reference_trajectory_msg.droneTrajectory.push_back(reference_position_msg);
  reference_trajectory_pub.publish(reference_trajectory_msg);

  reference_yaw_msg.yaw = estimated_pose_msg.yaw;
  reference_yaw_msg.header.stamp = ros::Time::now();
  yaw_controller_pub.publish(reference_yaw_msg);

  // Wait for controller to change mode
  ros::topic::waitForMessage<droneMsgsROS::droneTrajectoryControllerControlMode>(
    "droneTrajectoryController/controlMode", node_handle
  );

  // Send target point
  mission_point.x=target_position.x;
  mission_point.y=target_position.y;
  mission_point.z=target_position.z;
  mission_point_pub.publish(mission_point);

  header.frame_id = "behavior_go_to_point";

  msg.header = header;
  msg.command = droneMsgsROS::droneCommand::MOVE;
  controllers_pub.publish(msg);
  static_pose.x=estimated_pose_msg.x;
  static_pose.y=estimated_pose_msg.y;
  static_pose.z=estimated_pose_msg.z;
  


}

void BehaviorGoToPoint::ownRun()
{
  float pose_variation_maximum=0.2;
  float speed_maximum_at_end=0.1;
  if(is_finished==false)
  {
    if(std::abs(target_position.z-estimated_pose_msg.z)<pose_variation_maximum &&
    std::abs(target_position.x-estimated_pose_msg.x)<pose_variation_maximum &&
    std::abs(target_position.y- estimated_pose_msg.y)<pose_variation_maximum)
    {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
    if(timerIsFinished())
    {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }
    /*if(std::abs(target_position.z-estimated_pose_msg.z)<pose_variation_maximum &&
       std::abs(target_position.x-estimated_pose_msg.x)<pose_variation_maximum &&
       std::abs(target_position.y-estimated_pose_msg.y)<pose_variation_maximum &&
       estimated_speed_msg.dx>speed_maximum_at_end &&
       estimated_speed_msg.dz>speed_maximum_at_end &&
       estimated_speed_msg.dy>speed_maximum_at_end)
    {
      BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::WRONG_PROGRESS);
      BehaviorProcess::setFinishConditionSatisfied(true);
      is_finished = true;
      return;
    }*/
  }


}
std::tuple<bool,std::string> BehaviorGoToPoint::ownCheckActivationConditions()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador << "battery_level(self,LOW)";
  std::string query(capturador.str());
  query_service.request.query = query;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Battery low, unable to perform action");
    //return false;
  }
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,LANDED)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Drone landed");
    //return false;
  }

  return std::make_tuple(true,"");
  //return true;
}
void BehaviorGoToPoint::ownStop()
{
  // droneMsgsROS::dronePitchRollCmd pitch_roll_msg;
  // droneMsgsROS::droneDAltitudeCmd d_altitude_msg;
  // droneMsgsROS::droneDYawCmd d_yaw_msg;
  // droneMsgsROS::droneSpeeds reset;
  // reset.dx=0.0;
  // reset.dy=0.0;
  // reset.dz=0.0;
  // reset.dyaw=0.0;
  // reset.dpitch=0.0;
  // reset.droll=0.0;
  // pitch_roll_msg.pitchCmd = 0.0;
  // pitch_roll_msg.rollCmd = 0.0;
  // d_altitude_msg.dAltitudeCmd = 0.0;
  // d_yaw_msg.dYawCmd = 0.0;
  //
  // d_altitude_pub.publish(d_altitude_msg);
  // d_yaw_pub.publish(d_yaw_msg);
  // d_pitch_roll_pub.publish(pitch_roll_msg);
  // drone_speeds_reseter_pub.publish(reset);
  // std_msgs::Header header;
  // header.frame_id = "behavior_go_to_point";
  //
  // droneMsgsROS::droneCommand msg;
  // msg.header = header;
  // msg.command = droneMsgsROS::droneCommand::HOVER;
  // controllers_pub.publish(msg);


  estimated_pose_sub.shutdown();
  rotation_angles_sub.shutdown();
  estimated_speed_sub.shutdown();
  //staticity_timer.stop();
}
void BehaviorGoToPoint::estimatedSpeedCallback(const droneMsgsROS::droneSpeeds& msg)
{
  estimated_speed_msg=msg;
}
void BehaviorGoToPoint::estimatedPoseCallBack(const droneMsgsROS::dronePose& msg)
{
  estimated_pose_msg=msg;
}
void BehaviorGoToPoint::rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg)
{
  rotation_angles_msg=msg;
}
